
import { Component } from '@angular/core';
import { NavParams } from 'ionic-angular';
import { AstrologyEvent } from '../../providers/astrology-event';

import * as moment from 'moment'
import { JournalProvider, JournalEntry } from '../../providers/journal';

@Component({
  selector: 'page-journal',
  templateUrl: 'journal.html',
})
export class JournalPage {

  entries: JournalEntry[] = [];

  event: AstrologyEvent;

  constructor(private navParams: NavParams, private journalProvider: JournalProvider) {
    this.event = this.navParams.get('event');
  }

  ionViewDidEnter() {
    this.journalProvider.getJournalEntries().then(entries => this.entries = entries);
  }

  formatDate(date: Date): string { return moment(date).fromNow(); }
}
