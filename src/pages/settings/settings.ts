
import { Component } from '@angular/core';
import { SettingsProvider, NotificationSettings, NatalSettings } from '../../providers/settings';
import { AstrologyEventProvider } from '../../providers/astrology-event';
import { GoogleProvider, getCoordinateParts } from '../../providers/google';
import { LoadingController, Platform, NavController } from 'ionic-angular';
import { NotificationProvider } from '../../providers/notification';
import { getUTCOffsetString } from '../../providers/settings';

import debounce from 'lodash.debounce';
import moment from 'moment';

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})
export class SettingsPage {

  notificationSettings: NotificationSettings;
  natalSettings: NatalSettings;
  birthLocationMap: string;
  timezoneName: string;
  timezoneOffset: number;
  notificationMessages: string;

  constructor(private settingsProvider: SettingsProvider, private eventProvider: AstrologyEventProvider, private googleProvider: GoogleProvider, private loadingCtrl: LoadingController, private platform: Platform, private notificationProvider: NotificationProvider, private navCtrl: NavController) {
    this.notificationSettings = SettingsProvider.defaultSettings.notifications;
    this.natalSettings = SettingsProvider.defaultSettings.natal;
    this.platform.ready().then(() => this.setSettingsFromStorage());
  }

  async ngOnInit() {
    await this.platform.ready();
    if (this.platform.is('cordova')) {
      const notifications = await this.notificationProvider.getAllNotifications();
      if (notifications.length) {
        this.notificationMessages = notifications.map(n => `${n.text} at ${moment(n.at).format()}`).join(", \n");
      } else {
        this.notificationMessages = 'No notifications scheduled.'
      }
    } else {
      this.notificationMessages = 'No notifications enabled; not a cordova device.';
    }
  }

  /** Populate settings data from storage. */
  async setSettingsFromStorage() {
    const settings = await this.settingsProvider.getSettings();
    this.natalSettings = settings.natal;
    this.notificationSettings = settings.notifications;

    if (settings.natal.correctedBirthLocation) {
      const mapUrl = await this.googleProvider.getStaticMapsImageForLocation(settings.natal.birthLatitude, settings.natal.birthLongitude, 300, 150, false)
      this.birthLocationMap = mapUrl;
    }
  }

  async populateNatalEvents() {
    const settings = await this.settingsProvider.getSettings();
    if (settings.natal.birthDate && settings.natal.birthLatitude && settings.natal.birthLongitude && settings.natal.birthTimezoneOffset) {
      const birthDateString = settings.natal.birthDate + getUTCOffsetString(settings.natal.birthTimezoneOffset);
      const natalEventsResult = await this.eventProvider.populateNatalEvents(moment(birthDateString), settings.natal.birthLatitude, settings.natal.birthLongitude).toPromise();
      console.log('natal events result', natalEventsResult);
    }
  }

  /** Toggle the given category/field on or off, showing the given disabled or enabled message. */
  async toggleEnabled(value: boolean, category: string, field: string, enabledMessage: string, disabledMessage: string) {
    console.log(`toggle setting ${category}.${field} to: ${value}`);

    const loading = this.loadingCtrl.create({
      content: value ? enabledMessage : disabledMessage
    });

    loading.present();

    await this.settingsProvider.setSetting(category, field, value);
    await this.setSettingsFromStorage();

    await this.scheduleNotifications();

    loading.dismiss();
  }

  async scheduleNotifications(): Promise<any> {
    console.log('schedule notifications');
    const result = await this.notificationProvider.scheduleNotifications(this.navCtrl, await this.settingsProvider.getSettings());
    const notifications = await this.notificationProvider.getAllNotifications();
    this.notificationMessages = notifications.map(n => `${n.text} at ${moment(n.at, 'seconds').format()}`).join(", \n");
    console.log('scheduled synopsis: ', this.notificationMessages);

    return result;
  }

  async updateTimezoneIfPossible() {
    console.log('update time zone');
    const natal = this.natalSettings;
    if (natal.birthLatitude && natal.birthLongitude && natal.birthDate) {
      console.log('necessary information available');
      const timezone = await this.googleProvider.getTimezoneOffsetForLocation(moment.utc(natal.birthDate).toDate(), natal.birthLatitude, natal.birthLongitude).toPromise();
      console.log('timezone info', JSON.stringify(timezone));
      if (timezone) {
        this.timezoneName = timezone.timeZoneName;
        this.timezoneOffset = timezone.offsetInSeconds / 60 / 60;
        await this.settingsProvider.setSetting('natal', 'birthTimezoneOffset', timezone.offsetInSeconds);
        this.natalSettings = (await this.settingsProvider.getSettings()).natal;
      } else {
        this.timezoneName = '';
        this.timezoneOffset = null;
      }
    }
  }

  /** Called when the birth date and time is updated. */
  async birthDateUpdated(date: string) {
    const dateWithoutZ = date.substr(0, date.length - 1);
    console.log('birth date updated to ' + dateWithoutZ);
    await this.settingsProvider.setSetting('natal', 'birthDate', dateWithoutZ);
    await this.setSettingsFromStorage();
    await this.updateTimezoneIfPossible();
    await this.populateNatalEvents();
  }

  /** Debounced function. Called when the birth location text input is updated. */
  birthLocationUpdated = debounce(async (event: any) => {
    const location = event.target.value;
    console.log('birth location updated with ' + location);

    await this.settingsProvider.setSetting('natal', 'birthLocation', location);
    this.natalSettings.birthLocation = location;

    const coords = await this.googleProvider.getCoordinatesForAddress(location).toPromise();
    console.log(JSON.stringify(coords));
    if (coords) {
      await this.settingsProvider.setSetting('natal', 'correctedBirthLocation', coords.address);
      await this.settingsProvider.setSetting('natal', 'birthLatitude', coords.latitude);
      await this.settingsProvider.setSetting('natal', 'birthLongitude', coords.longitude);
      const mapUrl = this.googleProvider.getStaticMapsImageForLocation(coords.latitude, coords.longitude, 300, 150, coords.specific);
      this.birthLocationMap = mapUrl;
      await this.setSettingsFromStorage();
      await this.updateTimezoneIfPossible();
      await this.populateNatalEvents();
    }

  }, 1400, { leading: false, trailing: true })

  formatCoordinate(coordinate: number, plusLetter: string, minusLetter: string): string {
    const parts = getCoordinateParts(coordinate);
    return `${parts.degrees}° ${parts.minutes}' ${parts.seconds}" ${coordinate >= 0 ? plusLetter : minusLetter}`;
  }
}
