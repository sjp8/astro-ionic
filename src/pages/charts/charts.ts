import { Component } from '@angular/core';
import { EphemerisProvider, PlanetaryPosition } from '../../providers/ephemeris';
import moment from 'moment';
import { SettingsProvider } from '../../providers/settings';

@Component({
  selector: 'page-charts',
  templateUrl: 'charts.html',
})
export class ChartsPage {
  planets: PlanetaryPosition[];
  transits: PlanetaryPosition[];
  houses: number[];

  latitude: number;
  longitude: number;

  constructor(private ephemerisProvider: EphemerisProvider, private settingsProvider: SettingsProvider) { }

  ionViewDidLoad() {
    this.loadData();
  }

  async loadData() {
    const birthDate = await this.settingsProvider.getNatalBirthDate();
    const settings = await this.settingsProvider.getSettings();

    this.longitude = settings.natal.birthLongitude;
    this.latitude = settings.natal.birthLatitude;

    if (birthDate && settings.natal.birthLongitude && settings.natal.birthLatitude) {
      const results = await Promise.all([
        this.ephemerisProvider.getPositions(birthDate, this.longitude, this.latitude).toPromise(),
        this.ephemerisProvider.getHouses(birthDate, this.longitude, this.latitude).toPromise(),
        this.ephemerisProvider.getPositions(moment(), this.longitude, this.latitude).toPromise()
      ]);
      this.planets = results[0];
      this.houses = results[1];
      this.transits = results[2];
    } else {
      this.transits = await this.ephemerisProvider.getPositions(moment(), this.longitude, this.latitude).toPromise();
    }
  }

}
