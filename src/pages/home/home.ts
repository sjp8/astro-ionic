
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Content } from 'ionic-angular';

import { EventDetailPage } from '../event-detail/event-detail'
import { AstrologyEvent, AstrologyEventProvider, Astrology, LongitudeParts } from '../../providers/astrology-event'

import * as moment from 'moment'
import * as Color from 'color'
import { EphemerisProvider } from '../../providers/ephemeris';
import { SettingsProvider, NatalSettings } from '../../providers/settings';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  @ViewChild(Content) content: Content;

  eventType: 'natal' | 'transits';

  allEvents: AstrologyEvent[] = null
  natalSettings: NatalSettings;

  loading: boolean = false;
  selectedDate: Date = new Date();
  loadingMore: 'month' | 'year' = null;

  // events organized by section (moon or planetary) and dayIndex (see days).
  events: {
    [section: string]: {
      [dayIndex: number]: {
        date: moment.Moment,
        index: number,
        borderColor: string,
        event: AstrologyEvent
      }[]
    }
  };

  days: {
    index: number,
    date: moment.Moment,
    sunSign: string,
    moonSign: string,
    sunColor: string,
    moonColor: string,
    phase: {
      leftWidth: number,
      rightWidth: number,
      leftColor: string,
      rightColor: string
    }
  }[];

  reloadInterval: any;

  constructor(
    private navCtrl: NavController,
    private navParams: NavParams,
    private astrologyEvents: AstrologyEventProvider,
    private settingsProvider: SettingsProvider,
    private eventProvider: AstrologyEventProvider,
    private ephemerisProvider: EphemerisProvider)
  {
    this.events = null;
    this.eventType = this.navParams.get('eventType');
  }

  ionViewDidEnter() {

    this.selectedDate = moment().startOf('day').toDate();
    
    const getEvents = async () => {
      if (!this.allEvents || this.allEvents.length === 0) {
        const startDate = moment().startOf('month');
        const endDate = startDate.clone().endOf('month');
        const events = await this.getEvents(startDate, endDate, this.eventType);
        this.setEvents(events);
      }
    };

    this.reloadInterval = setInterval(getEvents, 60 * 1000);
    getEvents();
  }

  ionViewDidLeave() {
    clearInterval(this.reloadInterval);
  }

  ngOnDestroy() {
    clearInterval(this.reloadInterval);
  }

  /** Jump to event detail page when list item is touched. */
  eventSelected(event: AstrologyEvent) {
    this.navCtrl.push(EventDetailPage, { event })
  }

  private setEvents(events: AstrologyEvent[]) {
    if (events == null) {
      this.allEvents = null;
      return;
    }
    this.allEvents = events;

    const { days, events: organizedEvents } = this.organizeEventsByDay(events);

    this.events = organizedEvents;
    this.days = days;

    this.scrollToSelectedDate();
  }

  scrollToSelectedDate() {
    setTimeout(async () => {
      const selectedDateId = moment(this.selectedDate).format('YYYY-MM-DD');
      const element = document.getElementById(selectedDateId);
      console.log(`element ${selectedDateId} is ${element ? 'present' : 'not present'}`);
      element && element.previousElementSibling && element.previousElementSibling.scrollIntoView();
      element && element.scrollIntoView();
    }, 100);
  }

  getEventColumn(event: AstrologyEvent): 'planetary' | 'moon' {
    return event.planets.indexOf('Moon') !== -1 ? 'moon' : 'planetary';
  }

  /** Gets a list of events */
  async getEvents(startDate: moment.Moment, endDate: moment.Moment, index: 'natal' | 'transits'): Promise<AstrologyEvent[]> {
    this.loading = true;
    if (index === 'transits') {
      const events = await this.astrologyEvents.getEvents(startDate, endDate).toPromise();
      this.loading = false;
      return events || [];
    } else if (index === 'natal') {
      const natalSettings = (await this.settingsProvider.getSettings()).natal;
      this.natalSettings = natalSettings;
      const natalReady = this.hasBirthData();
      if (natalReady) {
        const birthDate = await this.settingsProvider.getNatalBirthDate(natalSettings);
        if (birthDate) {
          const events = await this.astrologyEvents.getNatalEvents(
            birthDate,
            natalSettings.birthLatitude,
            natalSettings.birthLongitude,
            startDate,
            endDate
          ).toPromise();
          this.loading = false;
          return events || [];
        } else {
          this.loading = false;
          return [];
        }
      } else {
        this.loading = false;
        return Promise.resolve([]);
      }
    }
  }

  getDateDescription(date: moment.Moment): string {
    const startOfEventDay = moment.utc(date.valueOf()).startOf('day')
    const startOfToday = moment.utc().startOf('day')
    const daysFromToday = startOfEventDay.diff(startOfToday, 'days')
    const values = {
      '-1': ' Yesterday',
      '0': ' Today',
      '1': ' Tomorrow'
    }
    const additional = values['' + daysFromToday] || ''
    return additional || startOfEventDay.from(startOfToday);
  }

  getColorForEventPlanet(event: AstrologyEvent, planet: string): string {
    const positions = event.getPositions();
    const position = positions.find(p => p.planet === planet);
    return this.getColorForSign(position.position.sign);
  }

  getColorForPlanetAndDate(planet: string, date: moment.Moment, endOfDay: boolean = true): string {
    const newDate = moment(date.valueOf());
    const lookupDate = endOfDay ? newDate.endOf('day') : newDate;
    const position = this.ephemerisProvider.getMoshierPositionForPlanet(planet, lookupDate);
    return this.getColorForSign(position.position.sign);
  }

  getColorForSign(sign: string): string {
    const colorStr = Astrology.getColorForSign(sign)
    const color = Color(colorStr).string();
    return color;
  }

  getPositionForPlanetAndDate(planet: string, date: moment.Moment): LongitudeParts {
    const lookup = moment(date.valueOf()).endOf('day');
    const position = this.ephemerisProvider.getMoshierPositionForPlanet(planet, lookup);
    return position.position;
  }

  getSignForPlanetAndDate(planet: string, date: moment.Moment): string {
    const lookup = moment(date.valueOf()).endOf('day');
    const position = this.ephemerisProvider.getMoshierPositionForPlanet(planet, lookup);
    return Astrology.getSign(position.position.sign);
  }

  colorToAlpha(color: string, alpha: number): string {
    return Color(color).alpha(alpha).string();
  }

  organizeEventsByDay(events: AstrologyEvent[]): { days: any[], events: { moon: any, planetary: any } } {
    const eventsData = events.map((event, index) => {
      const column = this.getEventColumn(event);
      return {
        index,
        event,
        date: event.date,
        borderColor: this.getColorForEventPlanet(event, column === 'moon' ? 'Moon' : 'Sun')
      };
    });

    // create map of events by day and column.
    const result: any = { moon: {}, planetary: {} };
    const days: { index: number, date: moment.Moment }[] = [];
    const dayAdded = {};
    eventsData.forEach((event, index) => {
      const dayIndex = event.date.year() * 1e3 + event.date.dayOfYear(); // year and day of year
      if (!dayAdded[dayIndex]) {
        const date = moment(event.date.valueOf()).startOf('day');
        const phase = this.ephemerisProvider.getMoshierPhase(date);
        console.log('phase name: ', phase);
        const sun = this.getPositionForPlanetAndDate('Sun', date);
        const day = {
          index: dayIndex,
          date,
          sunSign: Astrology.getSign(sun.sign),
          sunDegree: sun.degree,
          moonSign: this.getSignForPlanetAndDate('Moon', date),
          sunColor: this.getColorForPlanetAndDate('Sun', date, true),
          moonColor: this.getColorForPlanetAndDate('Moon', date, true),
          phase: {
            symbol:  Astrology.getPhase(phase.name),
            leftWidth: '' + (100 * (phase.lightIsOnLeft() ? phase.illuminatedPortion() : phase.darkPortion())) + '%',
            rightWidth: '' + (100 * (phase.lightIsOnRight() ? phase.illuminatedPortion() : phase.darkPortion())) + '%',
            leftColor: phase.lightIsOnLeft() ? 'white' : 'black',
            rightColor: phase.lightIsOnRight() ? 'white' : 'black'
          }
        };
        days.push(day);
        dayAdded[dayIndex] = true;
      }
      const column = this.getEventColumn(event.event);
      result[column][dayIndex] = result[column][dayIndex] || [];
      result[column][dayIndex].push(event);
    });

    return { days, events: result };
  }

  hasBirthData(): boolean {
    const natal = this.natalSettings;
    return natal && natal.birthDate && natal.birthLatitude && natal.birthLongitude && natal.birthTimezoneOffset != null;
  }

  async populateNatal() {
    const settings = await this.settingsProvider.getSettings();
    if (settings.natal.birthDate && settings.natal.birthLatitude && settings.natal.birthLongitude && settings.natal.birthTimezoneOffset) {
      const birthDate = await this.settingsProvider.getNatalBirthDate()
      const natalEventsResult = await this.eventProvider.populateNatalEvents(birthDate, settings.natal.birthLatitude, settings.natal.birthLongitude).toPromise();
      console.log('natal events result', natalEventsResult);
    }
  }

  async loadNext(group: 'year' | 'month', forward: boolean) {
    this.loadingMore = group;
    const multiplier = forward ? 1 : -1;
    await this.onChangeDate(moment(this.selectedDate).add(multiplier, group).toDate())
    this.loadingMore = null;
  }

  monthCache: { [yearAndMonth: string]: AstrologyEvent[] } = {};
  monthHistory: string[] = [];
  monthCacheSize = 6;
  addToCache = (dateId: string, events: AstrologyEvent[]) => {
    if (this.monthHistory.length > this.monthCacheSize) {
      const oldestId = this.monthHistory.shift();
      delete this.monthCache[oldestId];
    }
    this.monthHistory.push(dateId);
    this.monthCache[dateId] = events;
  }

  async onChangeDate(date: Date) {
    const sameMonth = date.getMonth() === this.selectedDate.getMonth() && date.getFullYear() === this.selectedDate.getFullYear();

    const previousDateId = moment(this.selectedDate).format('YYYY-MM-DD');
    this.selectedDate = date;
    const selectedDateId = moment(date).format('YYYY-MM-DD');

    // if it is the same date, no action.
    if (previousDateId === selectedDateId) {
      return;
    }

    // if it is the same month, only scroll
    if (sameMonth) {
      console.log('different date within same month.');
      this.scrollToSelectedDate();
    } else if (this.monthCache[selectedDateId]) {
      const events = this.monthCache[selectedDateId];
      if (previousDateId != selectedDateId) {
        this.setEvents(events);
        this.addToCache(selectedDateId, events);
      }
    } else {
      const startDate = moment(date).startOf('month');
      const endDate = startDate.clone().endOf('month');
      const events = await this.getEvents(startDate, endDate, this.eventType);
      this.setEvents(events);
      this.addToCache(selectedDateId, events);
    }
  }
}
