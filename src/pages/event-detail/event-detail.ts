
import { Component, ViewChild } from '@angular/core';
import { NavParams, NavController, LoadingController, ToastController, Content } from 'ionic-angular';
import { AstrologyEventProvider, AstrologyEvent, Astrology, AspectInfo } from '../../providers/astrology-event'
import { EphemerisProvider, PlanetaryPosition } from '../../providers/ephemeris'

import { Observable } from 'rxjs'

import * as moment from 'moment'
import * as momentTz from 'moment-timezone'
import { SettingsProvider, NatalSettings } from '../../providers/settings';
import { JournalAddPage } from '../journal-add/journal-add';

@Component({
  selector: 'page-event-detail',
  templateUrl: 'event-detail.html'
})
export class EventDetailPage {

  @ViewChild(Content) content: Content;

  viewEnterAnimationComplete: boolean = false

  event: AstrologyEvent
  data: any = {}
  date: string
  timezone: string
  time: string

  terms: string[];

  positions: PlanetaryPosition[]
  transits: PlanetaryPosition[]
  showTransits: boolean
  houses: number[]
  showHouses: boolean
  aspects: AspectInfo[]

  /** List of events to show in the timeline next to zodiac. */
  timelineEvents: AstrologyEvent[]

  /** Events to pass to the phases component as an argument (current phase and next 8 phases) */
  phaseEvents: AstrologyEvent[]

  /** Distance and Node moon events, for phase and moon details pages. */
  moonEvents: AstrologyEvent[]

  /** Retrograde event to show in retrograde details page. */
  retrogradeEvent: AstrologyEvent

  /** Direct event to show in retrograde details page. */
  directEvent: AstrologyEvent

  natalSettings: NatalSettings

  constructor(
    public navParams: NavParams,
    private navCtrl: NavController,
    private loading: LoadingController,
    private toastCtrl: ToastController,
    private astrologyEvents: AstrologyEventProvider,
    private ephemeris: EphemerisProvider,
    private settingsProvider: SettingsProvider
  ) {
    this.event = this.navParams.get('event') as AstrologyEvent
  }

  ionViewDidEnter() {
    this.setEvent(this.event);
    this.viewEnterAnimationComplete = true;
  }

  async setEvent(event: AstrologyEvent) {
    this.event = event;

    if (!event.data || !event.data.type) {
      console.debug('no event data or no event type set.');
    }
    console.debug('event: ' + event.getDescription(true) + ', ' + event.type);

    this.showTransits = this.showHouses = this.type === 'natal';
    if (this.type === 'natal') {
      const settings = await Promise.all([this.settingsProvider.getNatalBirthDate(), this.settingsProvider.getSettings()]);
      const natal = settings[1].natal;
      const birthDate = settings[0];

      this.ephemeris.getPositions(birthDate, natal.birthLongitude, natal.birthLatitude)
        .subscribe(positions => {
          this.positions = positions
          this.transits = event.getPositions();
        });
      this.ephemeris.getHouses(birthDate, natal.birthLongitude, natal.birthLatitude)
        .subscribe(houses => this.houses = houses);
    } else {
      this.positions = event.getPositions() || this.ephemeris.getMoshierPositions(event.date)
      this.aspects = this.getAspects(event, event.getPositions());
    }

    const countBefore = 3;
    const countAfter = this.type === 'phase' ? 9 : 3;

    if (!this.timelineEvents || this.timelineEvents.length === 0) {
      // if there are no sequential events yet, set one event as the default until more are loaded
      this.setTimelineEvents([event]);
    }
    this.astrologyEvents.getSequentialEvents(event.databaseId, this.type, countBefore, countAfter)
      .map(
        events => events,
        err => this.timelineEvents || [event]
      )
      .map(events => {
        if (this.type === 'phase' && events.length >= 2) {
          return this.astrologyEvents.getEvents(event.date, events[events.length - 1].date, 'moon')
            .map(moonEvents => ({ moonEvents, events }))
        } else {
          return Observable.of({ events, moonEvents: null })
        }
      })
      .flatMap(results => results)
      .subscribe(results => {
        this.setTimelineEvents(results.events);
        this.moonEvents = results.moonEvents;
      })
    
    this.data = event.data
    this.date = event.date.format('MMMM D, YYYY')
    this.terms = event.getTerms();

    const displayPlanet = this.getDisplayPlanet();
    if (displayPlanet) {
      // this.content.getScrollElement().style.background = 'url(assets/images/' + displayPlanet + '_background.png)';
    }
  
    this.time = event.date.format('h:mm a')
    this.timezone = momentTz.tz(event.date.valueOf(), momentTz.tz.guess()).format('z')
    if (this.type == 'retrograde') {
      this.data.direction = this.data.retrograde ? 'retrograde' : 'direct'
      this.data.nextDirection = this.data.retrograde ? 'direct' : 'retrograde'
    }

    if (this.data.nextEventLongitude) {
      this.data.nextEventPosition = Astrology.getLongitudeParts(this.data.nextEventLongitude)
    }
    if (this.data.longitude) {
      this.data.position = Astrology.getLongitudeParts(this.data.longitude)
    }

    if (this.data.averageFrequency) {
      const frequency = this.data.averageFrequency
      this.data.durations = {
        mean: this.formatSecondsAsDuration(frequency.mean),
        minimum: this.formatSecondsAsDuration(frequency.minimum),
        maximum: this.formatSecondsAsDuration(frequency.maximum)
      }
    }

    if (this.type == 'phase') {
      this.data.nextEventPhase = Astrology.getNextPhase(this.data.phase)
      this.data.nextEventAlternatePhase = Astrology.getAlternatePhase(this.data.nextEventPhase)
      this.data.alternatePhase = Astrology.getAlternatePhase(this.data.phase)
    } else if (this.type == 'aspect') {
      this.data.planets.forEach(planet => {
        planet.position = Astrology.getLongitudeParts(planet.longitude)
      })
    } else if (this.type == 'moon') {
      let distance = this.data.distance
      this.data.distance = {
        miles: Math.round(9.296e7 * distance),
        km: Math.round(1.496e8 * distance)
      }
    }

  }

  getDisplayPlanet(): string {
    return this.event.planets[0];
  }

  get type(): string {
    return this.event && this.event.type;
  }

  getPhaseEvents(): AstrologyEvent[] {
    return this.phaseEvents.filter(x => x);
  }

  setTimelineEvents(events: AstrologyEvent[]) {
    const eventIndex = events.findIndex(e => e.databaseId === this.event.databaseId);
    // 3 events before and after the selected event
    this.timelineEvents = events.filter((e, i) => i >= eventIndex - 3 && i <= eventIndex + 3);
    
    if (this.type === 'phase') {
      this.phaseEvents = events.filter(e => e.date.isSameOrAfter(this.event.date)) // this event and after
        .filter((e, i) => i < 8); // limit to 8 phase events
    } else if (this.type === 'retrograde') {
      this.directEvent = this.event.data.retrograde ? events[eventIndex + 1] : events[eventIndex];
      this.retrogradeEvent = this.event.data.retrograde ? events[eventIndex] : events[eventIndex - 1];
    }
  }
  
  get emphasizedPlanets(): string[] {
    if (this.event) {
      const data = this.event.data;
      return (data.planet ? [data.planet] : data.planets) || [];
    } else {
      return [];
    }
  }

  selectZodiacEvent(event: AstrologyEvent) {
    this.setEvent(event);
  }

  openEvent(eventId, type) {
    const loading = this.loading.create({ content: 'Loading Event...' })
    loading.present()
    this.astrologyEvents.getEvent(eventId, type)
      .catch((err) => Observable.of(null))
      .subscribe((event) => {
        if (event == null) {
          loading.dismiss()
          this.toastCtrl.create({
            duration: 3000,
            position: 'middle',
            message: "Could not retrieve event. Requires an active internet connection."
          }).present()
        } else {
          loading.dismiss()
          this.navCtrl.push(EventDetailPage, { event: event })
        }
      })
  }

  get eventDurationStr(): string {
    if (this.timelineEvents && ['sign', 'retrograde', 'phase'].indexOf(this.event.type) !== -1) {
      const eventIndex = this.timelineEvents.findIndex(e => e.databaseId === this.event.databaseId);
      const nextEventIndex = eventIndex + 1;
      if (this.timelineEvents[nextEventIndex]) {
        const diff = moment.duration(-this.event.date.diff(this.timelineEvents[nextEventIndex].date));
        return diff.humanize();
      }
    }

    return null;
  }

  formatDate(dateStr: string, short: boolean = false): string {
    const date = moment(dateStr)
    const format = short ? 'l' : 'MMMM D, YYYY [at] h:mm a'
    return date.format(format)
  }

  formatDuration(startDate: moment.Moment, endDate: moment.Moment): string {
    if (startDate && endDate) {
      return moment.duration(endDate.valueOf() - startDate.valueOf(), 'ms').humanize();
    } else {
      return '';
    }
  }

  formatSecondsAsDuration(seconds: number): string {
    const duration = moment.duration(seconds * 1000)
    const days = duration.days()
    const hours = duration.hours()
    return days == 0 ? `${hours} hours` : (days > 20 ? `${days} days` : `${days} days and ${hours} hours`)
  }

  getLongitudeParts(longitude: number) {
    return Astrology.getLongitudeParts(longitude)
  }

  round(num: number): number {
    return Math.round(num)
  }

  /**
   * Get aspects, re-ordering to prioritize planets matching the current event, and
   * placing the current event's planet as the first planet in the pair.
   */
  getAspects(event: AstrologyEvent, positions?: PlanetaryPosition[]): AspectInfo[] {
    const date = positions ? positions[0].date : event.date
    let aspects = Astrology.getAspects(positions || this.positions, this.ephemeris.getMoshierPositionsBefore(date))

    return aspects
  }

  openJournal() {
    this.navCtrl.push(JournalAddPage, {
      event: this.event
    })
  }

}
