
import { Component } from '@angular/core';
import { NavParams } from 'ionic-angular';
import { AstrologyEvent } from '../../providers/astrology-event';
import { JournalProvider, JournalEntry } from '../../providers/journal';

import { Device } from '@ionic-native/device';

import * as moment from 'moment'

@Component({
  selector: 'page-journal-add',
  templateUrl: 'journal-add.html',
})
export class JournalAddPage {

  event: AstrologyEvent;
  entry: JournalEntry;

  constructor(private navParams: NavParams, private device: Device, private journalProvider: JournalProvider) {
    this.event = this.navParams.get('event');
  }

  async ionViewDidEnter() {
    const entry = await this.journalProvider.getJournalEntry(this.event.databaseId);
    this.entry = entry || (await this.journalProvider.createJournalEntry(this.event));
  }

  formatDate(date: Date): string { return moment(date).fromNow(); }

  selectedItemIndex = 0;

}
