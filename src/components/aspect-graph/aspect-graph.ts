import { Component, Input, ElementRef, ViewChild, SimpleChanges } from '@angular/core'

import { AstrologyEvent, Astrology, AstrologyEventProvider } from '../../providers/astrology-event'
import { PaperAspectGraph } from '../../providers/paper-aspect-graph';
import { EphemerisProvider, PlanetaryPosition, PositionsOverTime } from '../../providers/ephemeris';

import * as moment from 'moment'
import paper from 'paper'
import { Observable } from 'rxjs';

@Component({
  selector: 'aspect-graph',
  templateUrl: 'aspect-graph.html'
})
export class AspectGraphComponent {

  @Input() event: AstrologyEvent;
  @Input() timelineEvents: AstrologyEvent[];

  ephemeris: PositionsOverTime

  @ViewChild('aspectCanvas') canvas: ElementRef;
  get element(): HTMLCanvasElement { return this.canvas && this.canvas.nativeElement; }

  paperAspectGraph: PaperAspectGraph;

  constructor(private ephemerisProvider: EphemerisProvider, private eventProvider: AstrologyEventProvider) {
  }

  ngOnInit() {
    this.getData().subscribe(result => this.setupGraph(result.ephemeris, result.retrogrades));
  }

  ngOnDestroy() {
    this.paperAspectGraph && this.paperAspectGraph.destroy();
    this.paperAspectGraph = null;
    this.ephemeris = null;
  }

  getData(): Observable<{ ephemeris: PositionsOverTime, retrogrades: AstrologyEvent[] }> {
    const eventRange = this.determineRange();
    const diff = Math.abs(eventRange.beforeAspect.diff(eventRange.afterAspect, 'day'));
    const padding = diff / 4;
    const dates = {
      min: moment(eventRange.beforeAspect.valueOf()).add(-padding, 'day'),
      max: moment(eventRange.afterAspect.valueOf()).add(padding, 'day')
    };

    const planets = this.event.data.planets.map(p => p.name);

    const getEphemeris = this.ephemerisProvider.getEphemeris(dates.min, dates.max, diff < 80 ? 'day' : 'week', this.event.data.planets.map(p => p.name));
    
    const getRetrogrades = this.eventProvider.getEvents(dates.min, dates.max, 'retrograde')
      .map(events => events.filter(e => planets.indexOf(e.data.planet) !== -1))

    return Observable.forkJoin(getEphemeris, getRetrogrades)
      .map(results => ({ ephemeris: results[0], retrogrades: results[1] }));
  }

  setupGraph(ephemeris: PositionsOverTime, retrogrades: AstrologyEvent[]) {
    const scope = new paper.PaperScope();
    scope.setup(this.element);

    this.paperAspectGraph = new PaperAspectGraph(
      scope,
      this.event,
      this.timelineEvents,
      retrogrades,
      ephemeris
    );
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.paperAspectGraph) {
      this.getData().subscribe(result => this.paperAspectGraph.setEvent(this.event, this.timelineEvents, result.retrogrades, result.ephemeris));
    }
  }

  determineRange(): { beforeAspect: moment.Moment, afterAspect: moment.Moment } {
    const planets = this.event.data.planets.map(p => p.name);

    const seekUntil = (context: any, startDate: moment.Moment, step: string, direction: 'forward' | 'backward', until: { (context: any, date: moment.Moment, positions: PlanetaryPosition[]): boolean }): moment.Moment => {
      const date = moment(startDate.valueOf());
      let positions: PlanetaryPosition[];
      let iterations = 0;
      while (!positions || !until(context, date, positions)) {
        positions = this.ephemerisProvider.getMoshierPositions(date);
        date.add(moment.duration(direction === 'forward' ? 1 : -1, iterations < 40 ? 'day' : (iterations < 52 ? 'week' : 'month')));
        iterations++;
      }

      return date;
    }

    const values = { beforeAspect: null, afterAspect: null };

    const startAt = this.event.date;

    const originalAngle = Astrology.getAngle(this.event.data.planets[0].longitude, this.event.data.planets[1].longitude);
    const originalAspect = Astrology.getAspectDefinition(this.event.data.aspect);
    const targetAngles = {
      min: originalAspect ? originalAspect.angle - originalAspect.orb * 2 : originalAngle - 15,
      max: originalAspect ? originalAspect.angle + originalAspect.orb * 2 : originalAngle + 15
    };

    values.beforeAspect = seekUntil(null, startAt, 'week', 'backward', (context, date, positions) => {
      const planet1 = positions.find(p => p.planet === planets[0]);
      const planet2 = positions.find(p => p.planet === planets[1]);
      const angle = Astrology.getAngle(planet1.longitude, planet2.longitude);
      return angle <= targetAngles.min || angle >= targetAngles.max;
    });

    values.afterAspect = seekUntil(null, startAt, 'week', 'forward', (context, date, positions) => {
      const planet1 = positions.find(p => p.planet === planets[0]);
      const planet2 = positions.find(p => p.planet === planets[1]);
      const angle = Astrology.getAngle(planet1.longitude, planet2.longitude);
      return angle >= targetAngles.max || angle <= targetAngles.min;
    });

    return values;
  }
}
