import { NgModule } from '@angular/core';
import { ZodiacComponent } from './zodiac/zodiac';
import { PositionsComponent } from './positions/positions';
import { DescriptionModule } from './description/description.module';
import { PhasesComponent } from './phases/phases';
import { RetrogradeGraphComponent } from './retrograde-graph/retrograde-graph';
import { AspectGraphComponent } from './aspect-graph/aspect-graph';
import { SequentialEventsComponent } from './sequential-events/sequential-events';
import { TermsComponent } from './terms/terms';
import { TimelineEventComponent } from './timeline-event/timeline-event';
import { DateSelectComponent } from './date-select/date-select';

import { IonicModule } from 'ionic-angular';
import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

@NgModule({
  declarations: [
    ZodiacComponent,
    PositionsComponent,
    RetrogradeGraphComponent,
    AspectGraphComponent,
    SequentialEventsComponent,
    TermsComponent,
    TimelineEventComponent,
    DateSelectComponent,
    PhasesComponent
  ],
  imports: [
    DescriptionModule,
    IonicModule,
    CommonModule
  ],
  exports: [
    ZodiacComponent,
    PositionsComponent,
    RetrogradeGraphComponent,
    AspectGraphComponent,
    SequentialEventsComponent,
    PhasesComponent,
    TermsComponent,
    TimelineEventComponent,
    DateSelectComponent,
    DescriptionModule
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class ComponentsModule { }
