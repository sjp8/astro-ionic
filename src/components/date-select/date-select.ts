
import { Component, Input, Output, EventEmitter } from '@angular/core'

import * as moment from 'moment';

@Component({
  selector: 'date-select',
  templateUrl: 'date-select.html'
})
export class DateSelectComponent {
  @Input() date: Date;
  @Output() dateChanged = new EventEmitter<Date>();

  years = [2017, 2018, 2019];
  months = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];
  days = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30];

  monthDisplayValues = this.months.map(m => moment().month(m).format('MMM'));

  setDays(year: number, month: number) {
    const date = moment();
    date.year(year);
    date.month(month);
    this.days = new Array(date.daysInMonth()).fill(0).map((_, i) => i + 1);
    console.log('set days to ' + this.days.join(", "));
  }

  onChangeYear(year: number) {
    console.log('Year changed to: ' + year);
    const newDate = moment.utc(this.date);
    newDate.year(year);
    this.setDays(newDate.year(), newDate.month());

    this.years = [year - 1, year, year + 1];

    this.dateChanged.emit(newDate.toDate());
  }

  onChangeMonth(month: number) {
    console.log('Month changed to: ' + month);
    const newDate = moment(this.date);
    newDate.month(month);

    this.setDays(newDate.year(), newDate.month());

    this.dateChanged.emit(newDate.toDate());
  }

  onChangeDay(day: number) {
    console.log('Day changed to: ' + day);
    const newDate = moment(this.date);
    newDate.date(day);

    this.dateChanged.emit(newDate.toDate());
  }
}
