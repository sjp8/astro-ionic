import { Component, Input, EventEmitter, Output } from '@angular/core';
import { AstrologyEvent, Astrology } from '../../providers/astrology-event';

@Component({
  selector: 'timeline-event',
  templateUrl: 'timeline-event.html'
})
export class TimelineEventComponent {

  @Input() event: AstrologyEvent;
  @Input() showDate: boolean = false;
  @Output() eventSelected = new EventEmitter<any>();

  constructor() {
  }

  getBackgroundImageCss(event: AstrologyEvent, index: 0 | 1): string {
    const planet: string = event.planets[index];
    return planet && `url(assets/images/thumbnails/${planet.toLowerCase()}.png)`;
  }

  getSymbol(event: AstrologyEvent, type: string = event.type): string {
    if (type === 'aspect') {
      return Astrology.getAspect(event.data.aspect);
    } else if (type === 'sign') {
      return Astrology.getSign(event.data.sign);
    } else if (type === 'retrograde') {
      return event.data.retrograde ? '↺' : '↻';
    } else if (type === 'phase') {
      return Astrology.getPhase(event.data.phase);
    } else if (type === 'moon') {
      return Astrology.moonNodeCharacters[event.data.event] || null;
    } else if (type === 'natal') {
      if (event.data.event === 'aspect') {
        const data = event.data;
        const aspect = Astrology.getAspectForAngle(Astrology.getAngle(data.natalPlanet.longitude, data.transitingPlanet.longitude));
        event.data.aspect = aspect || event.data.aspect;
        return this.getSymbol(event, 'aspect');
      } else if (event.data.event === 'house') {
        return event.data.natalHouse;
      }
    }
    return null;
  }

  getSymbolColor(event: AstrologyEvent, type: string = event.type): string {
    if (type === 'aspect') {
      const aspect = Astrology.getAspectDefinition(event.data.aspect);
      return aspect && aspect.color;
    } else if (type === 'sign') {
      return Astrology.getColorForSign(event.data.sign);
    } else if (type === 'retrograde') {
      return 'inherit';
    } else if (type === 'phase') {
      return 'inherit';
    } else if (type === 'moon') {
      return 'inherit';
    } else if (type === 'natal') {
      return this.getSymbolColor(event, event.data.event);
    }
    return null;
  }

}