import { Component, Input, ElementRef, ViewChild, SimpleChanges } from '@angular/core'
import { Astrology, AstrologyEvent } from '../../providers/astrology-event'
import * as moment from 'moment'
import { PaperMoon } from '../../providers/paper-moon';
import paper from 'paper'

@Component({
  selector: 'phases',
  templateUrl: 'phases.html'
})
export class PhasesComponent {

  @Input() phases: AstrologyEvent[]
  // moon events during the cycle (from current phase until start of this phase in next cycle)
  @Input() moonEvents: AstrologyEvent[]
  @Input() date: moment.Moment

  @ViewChild('phasesCanvas') canvas: ElementRef;
  get element(): HTMLCanvasElement { return this.canvas && this.canvas.nativeElement; }

  scope: paper.PaperScope;
  paperMoon: PaperMoon;

  getPositionData(): { planet: string, sign: string, degree: number, minute: number }[] {
    return this.phases ? this.phases.map(p => {
      const data = Astrology.getLongitudeParts(p.data.longitude)
      return { ...data, planet: p.data.planet }
    }) : []
  }

  ngOnInit() {
    this.setupPhases();
  }

  setupPhases() {
    const scope = new paper.PaperScope();
    scope.setup(this.element);
    this.paperMoon = new PaperMoon(scope, this.phases, this.moonEvents);
    this.scope = scope;
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.paperMoon) {
      if (changes.phases && changes.moonEvents) {
        this.paperMoon.setPhases(changes.phases.currentValue, changes.moonEvents.currentValue);
      } else if (changes.phases) {
        this.paperMoon.setPhases(changes.phases.currentValue, this.moonEvents);
      } else if (changes.moonEvents) {
        this.paperMoon.setPhases(this.phases, changes.moonEvents.currentValue);
      }
    }
  }
}
