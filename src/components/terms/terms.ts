import { Component, Input } from '@angular/core';
import { TermsProvider } from '../../providers/terms';

@Component({
  selector: 'terms',
  templateUrl: 'terms.html'
})
export class TermsComponent {

  @Input() terms: string[];

  uniqueTerms: string[];
  adjectivesByTerm: { [term: string]: string[] } = {};
  aliasesByTerm: { [term: string]: string[] } = {};
  factsByTerm: { [term: string]: string[] } = {};
  articlesByTerm: { [term: string]: string[] } = {};

  constructor(private termsProvider: TermsProvider) {}

  ngOnInit() {
    this.uniqueTerms = this.terms.filter((t, i) => this.terms.slice(i + 1).indexOf(t) === -1);
    false && this.termsProvider.getDictionary()
      .subscribe(dictionary => {
        this.adjectivesByTerm = {};
        this.articlesByTerm = {};
        this.factsByTerm = {};
        this.aliasesByTerm = {};
        this.uniqueTerms
          .map(name => ({ name, term: dictionary.getTermObject(name) }))
          .filter(row => row.term != null)
          .forEach(row => {
            const { name, term } = row;
            const terms = this.uniqueTerms.filter(t => t != name);
            this.adjectivesByTerm[name] = this.randomize(term.getAdjectives(terms));
            this.aliasesByTerm[name] = term.names.filter(n => n != name);
            this.factsByTerm[name] = term.facts;
            this.articlesByTerm[name] = term.articles;
          });
      });
  }

  randomize(ary: string[], limit: number = 4): string[] {
    const copy = ary.map(x => x);
    copy.sort((a, b) => Math.random() - 0.5);
    return copy.filter((e, i) => i < limit);
  }
}
