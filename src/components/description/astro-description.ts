
import { Component, Input } from '@angular/core'
import { Astrology, AstrologyEvent } from '../../providers/astrology-event'

@Component({
  selector: 'astro-description',
  templateUrl: 'astro-description.html'
})
export class AstrologyDescriptionComponent {
  @Input() event: AstrologyEvent
  @Input() public set text(value: boolean) { this._utf8 = !(value == null || value) }
  private _utf8: boolean = true

  public get text(): boolean { return !this._utf8 }
  public get utf8(): boolean { return this._utf8 }

  char(item: string): string {
    return Astrology.getAspect(item) || Astrology.getPlanet(item) ||
      Astrology.getSign(item) || Astrology.getPhase(item)
  }

  element(sign: string): string {
    return Astrology.getElementForSign(sign).toLowerCase()
  }

  get data(): any {
    return this.event ? this.event.data : null
  }

  getAspect(event: AstrologyEvent): string {
    if (event.type === 'natal' && event.data.event === 'aspect') {
      const aspect = Astrology.getAspectForAngle(Astrology.getAngle(event.data.transitingPlanet.longitude, event.data.natalPlanet.longitude));
      return aspect;
    }
  }
}