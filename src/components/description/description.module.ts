import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';  
import { AstrologyDescriptionComponent } from './astro-description';
import { AstrologyItemComponent } from './astro-item';

@NgModule({
  declarations: [
    AstrologyDescriptionComponent,
    AstrologyItemComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    AstrologyDescriptionComponent,
    AstrologyItemComponent
  ],
  providers: []
})
export class DescriptionModule {}
