
import { Component, Input } from '@angular/core'
import { Astrology } from '../../providers/astrology-event'

@Component({
  selector: 'astro-item',
  templateUrl: 'astro-item.html'
})
export class AstrologyItemComponent {
  @Input() planet: string
  @Input() retrograde: boolean
  @Input() direct: boolean
  @Input() phase: string
  @Input() aspect: string
  @Input() sign: string
  @Input() degree: number // corresponds to sign degree
  @Input() minute: number // corresponds to sign minute
  @Input() moon: string

  /** Show the text version of the value (if left out, shows the utf-8 character). */
  @Input() public set text(value: boolean) { this._utf8 = !(value == null || value) }
  private _utf8: boolean = true

  /** Show both  */
  @Input() public set both(value: string) { this.pair = true }
  private pair: boolean = false

  /** Show the utf-8 character? */
  public get utf8(): boolean { return this.pair || this._utf8 }

  /** Show the text value? */
  public get text(): boolean { return this.pair || !this._utf8 }

  symbol(item: string, type: string) {
    return (type == 'planet' && Astrology.getPlanet(item)) || (type == 'aspect' && Astrology.getAspect(item)) ||
      (type == 'sign' && Astrology.getSign(item)) || (type == 'phase' && Astrology.getPhase(item))
  }

  getElement(sign: string): string {
    return Astrology.getElementForSign(sign).toLowerCase()
  }

  moonEvent(event: string, utf8: boolean) {
    const types = {
      an: utf8 ? '☊' : 'Ascending Node',
      dn: utf8 ? '☋' : 'Descending Node',
      perigee: 'Perigee',
      apogee: 'Apogee',
      voc: utf8 ? 'VOC' : 'Void of Course'
    }
    if (types[event]) {
      return types[event]
    } else {
      console.error('Unknown moon event type: ' + event)
      return 'unknown'
    }
  }
}
