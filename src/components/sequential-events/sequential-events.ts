
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { AstrologyEvent } from '../../providers/astrology-event';

@Component({
  selector: 'sequential-events',
  templateUrl: 'sequential-events.html'
})
export class SequentialEventsComponent {

  @Input() events: AstrologyEvent[];
  @Input() selectedEvent: AstrologyEvent;
  @Output() eventSelected = new EventEmitter<AstrologyEvent>();

  selectZodiacEvent(event: AstrologyEvent) {
    if (!this.selectedEvent || event.databaseId !== this.selectedEvent.databaseId) {
      this.eventSelected.emit(event);
    }
  }

}
