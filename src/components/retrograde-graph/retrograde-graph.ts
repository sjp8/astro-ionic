import { Component, Input, ElementRef, ViewChild, SimpleChanges } from '@angular/core'

import { AstrologyEvent } from '../../providers/astrology-event'
import { PaperRetrogradeGraph } from '../../providers/paper-retrograde-graph';
import { EphemerisProvider, PlanetaryPosition, PositionsOverTime } from '../../providers/ephemeris';

import * as moment from 'moment'
import paper from 'paper'
import { Observable } from 'rxjs';

@Component({
  selector: 'retrograde-graph',
  templateUrl: 'retrograde-graph.html'
})
export class RetrogradeGraphComponent {

  @Input() retrogradeEvent: AstrologyEvent;
  @Input() directEvent: AstrologyEvent;

  ephemeris: PositionsOverTime

  @ViewChild('retrogradeCanvas') canvas: ElementRef;
  get element(): HTMLCanvasElement { return this.canvas && this.canvas.nativeElement; }

  scope: paper.PaperScope;
  paperRetrogradeGraph: PaperRetrogradeGraph;

  constructor(private ephemerisProvider: EphemerisProvider) {
  }

  ngOnInit() {
    this.getData().subscribe(() => this.setupGraph());
  }

  ngOnDestroy() {
    this.paperRetrogradeGraph && this.paperRetrogradeGraph.destroy();
    this.paperRetrogradeGraph = null;
    this.ephemeris = null;
  }

  getData(): Observable<any> {
    const eventRange = this.determineRange();
    const diff = Math.abs(this.directEvent.date.diff(this.retrogradeEvent.date, 'day'));
    const padding = diff / 4;
    const dates = {
      min: moment(eventRange.enterRx.valueOf()).add(-padding, 'day'),
      max: moment(eventRange.leaveRx.valueOf()).add(padding, 'day')
    };
    return this.ephemerisProvider.getEphemeris(dates.min, dates.max, diff < 80 ? 'day' : 'week', [this.directEvent.data.planet])
      .do(result => this.ephemeris = result);
  }

  setupGraph() {
    const scope = new paper.PaperScope();
    scope.setup(this.element);

    this.paperRetrogradeGraph = new PaperRetrogradeGraph(
      scope,
      this.retrogradeEvent,
      this.directEvent,
      this.ephemeris
    );
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.paperRetrogradeGraph) {
      this.getData().subscribe(() => this.paperRetrogradeGraph.setEvent(this.retrogradeEvent, this.directEvent, this.ephemeris));
    }
  }

  determineRange(): { enterRx: moment.Moment, retrograde: moment.Moment, direct: moment.Moment, leaveRx: moment.Moment } {
    const planet = this.retrogradeEvent.data.planet;

    const seekUntil = (context: any, startDate: moment.Moment, step: string, direction: 'forward' | 'backward', until: { (context: any, date: moment.Moment, longitude: number): boolean }): moment.Moment => {
      const date = moment(startDate.valueOf());
      let position: PlanetaryPosition = null;
      while (!position || !until(context, date, position.longitude)) {
        position = this.ephemerisProvider.getMoshierPositionForPlanet(planet, date);
        date.add(moment.duration(direction === 'forward' ? 1 : -1, step as any));
      }

      return date;
    }

    const values = { enterRx: null, retrograde: this.retrogradeEvent.date, direct: this.directEvent.date, leaveRx: null };

    const directLongitude = this.directEvent.data.longitude;
    const retrogradeLongitude = this.retrogradeEvent.data.longitude;

    // then go forward in time, continuing from direct event date, until retrograde event longitude
    // (this is where the planet left the retrograde zone).
    values.leaveRx = seekUntil(null, values.direct, 'day', 'forward', (context, date, longitude) => {
      return longitude > retrogradeLongitude;
    });

    // go backward in time, starting at retrograde date, until the position of the direct event is passed
    // (this is where the planet entered the retrograde zone).
    values.enterRx = seekUntil(null, values.retrograde, 'day', 'backward', (context, date, longitude) => {
      return longitude < directLongitude;
    });

    return values;
  }
}
