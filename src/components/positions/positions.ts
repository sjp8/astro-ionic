import { Component, Input } from '@angular/core';
import { PlanetaryPosition } from '../../providers/ephemeris'
import { Astrology } from '../../providers/astrology-event'
import * as moment from 'moment'

@Component({
  selector: 'positions',
  templateUrl: 'positions.html'
})
export class PositionsComponent {

  @Input() positions: PlanetaryPosition[]
  @Input() highlights: string[] = [] // highlighted planets (by long name).
  @Input() date: moment.Moment
  @Input() title: string

  getPositionData(): { planet: string, sign: string, degree: number, minute: number, retrograde: boolean }[] {
    return this.positions ? this.positions.map(p => {
      const data = Astrology.getLongitudeParts(p.longitude)
      return { ...data, planet: p.planet, retrograde: p.retrograde }
    }) : []
  }
}
