
import { Component, Input, Output, ViewChild, ElementRef, SimpleChanges, EventEmitter } from '@angular/core';
import * as paper from 'paper';
import { EphemerisProvider, PlanetaryPosition } from '../../providers/ephemeris';
import { PaperZodiac } from '../../providers/paper-zodiac';

@Component({
  selector: 'zodiac',
  templateUrl: 'zodiac.html'
})
export class ZodiacComponent {

  @Input() positions: PlanetaryPosition[];
  @Input() transits: PlanetaryPosition[];
  @Input() houses: number[];
  @Input() emphasizedPlanets: string[] = [];

  @Output() animatePositions = new EventEmitter<PlanetaryPosition[]>();

  @ViewChild('zodiacCanvas') canvas: ElementRef;

  scope: paper.PaperScope;
  paperZodiac: PaperZodiac;

  get element(): HTMLCanvasElement { return this.canvas && this.canvas.nativeElement; }

  constructor(private ephemeris: EphemerisProvider) {
  }

  ngOnInit() {
    this.setupZodiac();
  }

  ngOnDestroy() {
    this.paperZodiac && this.paperZodiac.destroy();
    this.paperZodiac = null;
  }

  setupZodiac() {
    console.debug('setting up zodiac');
    const scope = new paper.PaperScope();
    scope.setup(this.element);
    this.paperZodiac = new PaperZodiac(scope,  this.positions, this.houses, this.transits, this.emphasizedPlanets);

    this.scope = scope;
  }

  private setPositionsTimer: any;

  ngOnChanges(changes: SimpleChanges) {
    if ((changes.positions || changes.transits) && this.paperZodiac) {
      this.setPositionsTimer && clearTimeout(this.setPositionsTimer);
      this.setPositionsTimer = setTimeout(() => {
        console.log('zodiac component detected changes, updating paper zodiac');
        this.paperZodiac.setPositions(this.positions, this.transits, date => this.ephemeris.getMoshierPositions(date));
      }, 100);
    }
  }

}
