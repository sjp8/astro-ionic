import { NgModule, ErrorHandler, Injectable, Injector } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { HttpModule } from '@angular/http'

import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular'

import { MyApp } from './app.component'
import { HomePage } from '../pages/home/home'
import { EventDetailPage } from '../pages/event-detail/event-detail'
import { SettingsPage } from '../pages/settings/settings';
import { ChartsPage } from '../pages/charts/charts';
import { JournalPage } from '../pages/journal/journal';
import { JournalAddPage } from '../pages/journal-add/journal-add';

import { ProvidersModule } from '../providers/providers.module';
import { ComponentsModule } from '../components/components.module';


@Injectable()
export class AstroErrorHandler implements ErrorHandler {
  ionicErrorHandler: IonicErrorHandler;

  constructor(injector: Injector) {
    try {
      this.ionicErrorHandler = injector.get(IonicErrorHandler);
    } catch(e) {
      // Unable to get the IonicErrorHandler provider, ensure
      // IonicErrorHandler has been added to the providers list below
    }
  }

  handleError(err: any): void {
    // Remove this if you want to disable Ionic's auto exception handling
    // in development mode.
    this.ionicErrorHandler && this.ionicErrorHandler.handleError(err);
  }
}

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    EventDetailPage,
    SettingsPage,
    ChartsPage,
    JournalPage,
    JournalAddPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    ComponentsModule,
    ProvidersModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    EventDetailPage,
    SettingsPage,
    ChartsPage,
    JournalPage,
    JournalAddPage
  ],
  providers: [
    IonicErrorHandler,
    [{ provide: ErrorHandler, useClass: AstroErrorHandler }]
  ]
})
export class AppModule {}
