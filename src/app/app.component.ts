import { Component, ViewChild } from '@angular/core';
import { Platform, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { SettingsPage } from '../pages/settings/settings';
import { ChartsPage } from '../pages/charts/charts';
import { JournalPage } from '../pages/journal/journal';

import { NotificationProvider } from '../providers/notification';
import { SettingsProvider } from '../providers/settings';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  tab1 = HomePage;
  tab1Params = { eventType: 'transits'};
  tab2 = HomePage;
  tab2Params = { eventType: 'natal' };
  tab3 = SettingsPage;
  tab4 = ChartsPage;
  tab5 = JournalPage;
  tab5Params = { mode: 'view', event: null };

  @ViewChild(Nav) nav: Nav;

  constructor(
    private platform: Platform,
    private statusBar: StatusBar,
    private splashScreen: SplashScreen,
    private notificationProvider: NotificationProvider,
    private settingsProvider: SettingsProvider
  ) {
    this.setup();
  }

  async setup() {
    await this.platform.ready();
    if (this.platform.is('cordova')) {
      this.cordovaSettings();
      await this.notificationProvider.scheduleNotifications(this.nav, await this.settingsProvider.getSettings());
    }
  }

  cordovaSettings() {
    this.statusBar.styleDefault();
    this.statusBar.overlaysWebView(false);
    this.statusBar.backgroundColorByHexString('#ffffff');
    this.splashScreen.hide();
    this.statusBar.show();
  }

}
