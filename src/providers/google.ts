
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';

import Config from './config';
import { getUTCOffsetString } from './settings';

@Injectable()
export class GoogleProvider {

  constructor(private http: Http) { }

  getStaticMapsImageUrlForAddress(address: string, width: number = 300, height: number = 300, closeZoom: boolean = false): string {
    const escapedAddress = encodeURIComponent(address);
    const url = `https://maps.googleapis.com/maps/api/staticmap?center=${escapedAddress}&zoom=${closeZoom ? 12 : 8}&size=${width}x${height}&maptype=hybrid&key=${Config.google.apiKey}`;
    return url
  }

  getStaticMapsImageForLocation(latitude: number, longitude: number, width: number = 300, height: number = 300, closeZoom: boolean = false): string {
    return this.getStaticMapsImageUrlForAddress(`${latitude},${longitude}`, width, height, closeZoom);
  }

  getCoordinatesForAddress(address: string): Observable<{ address: string, latitude: number, longitude: number, specific: boolean }> {
    if (!address) {
      return Observable.of(null);
    }

    const escapedAddress = encodeURIComponent(address);
    const url = `https://maps.googleapis.com/maps/api/geocode/json?address=${escapedAddress}&key=${Config.google.apiKey}`;
    return this.http.get(url)
      .map(res => res.json())
      .map(json => {
        const result = json.results[0];
        if (!result) {
          return null;
        }
        
        const formattedAddress = result.formatted_address;
        const latitude = result.geometry.location.lat;
        const longitude = result.geometry.location.lng;
        const specific = result.geometry.location_type !== 'APPROXIMATE';
        
        return {
          address: formattedAddress, latitude, longitude, specific
        }
      })
      .catch(res => Observable.of(null))
  }

  getTimezoneOffsetForLocation(date: Date, latitude: number, longitude: number): Observable<{ timeZoneId: string, timeZoneName: string, offsetInSeconds: number, dst: boolean, utcOffsetString: string }> {
    const timestamp = Math.floor(date.getTime() / 1000);
    const url = `https://maps.googleapis.com/maps/api/timezone/json?location=${latitude},${longitude}&timestamp=${timestamp}&key=${Config.google.apiKey}`;
    return this.http.get(url)
      .map(res => res.json())
      .map(json => {
        const timeZoneId = json.timeZoneId;
        const timeZoneName = json.timeZoneName;
        const dst = json.dstOffset !== 0;
        const offsetInSeconds = json.rawOffset + json.dstOffset;
        const utcOffsetString = getUTCOffsetString(offsetInSeconds);

        return {
          timeZoneId, timeZoneName, dst, offsetInSeconds, utcOffsetString
        }
      })
  }
}

export const getCoordinateParts = (coordinate: number): { degrees: number, minutes: number, seconds: number, positive: boolean } => {
  const degreesWithDecimal = Math.abs(coordinate);
  const degrees = Math.floor(degreesWithDecimal)
  const minutesWithDecimal = (degreesWithDecimal - degrees) * 60
  const minutes = Math.floor(minutesWithDecimal) // how far through this degree (as a portion of 60)
  const seconds = Number(((minutesWithDecimal - minutes) * 60).toFixed(3)) // how far through the portion of 60 (as a portion of 60)

  return { degrees, minutes, seconds, positive: coordinate >= 0 };
}
