const useLocalServer = false;

const Config = {
  termFiles: [
    "Apogee.txt", "Aquarius.txt", "Aries.txt", "Ascending Node.txt", "Cancer.txt", "Capricorn.txt", "Conjunction.txt", "Descending Node.txt", "Direct.txt", "Gemini.txt", "Jupiter.txt", "Leo.txt", "Libra.txt", "Mars.txt", "Mercury.txt", "Moon.txt", "Neptune.txt", "Opposition.txt", "Perigee.txt", "Pisces.txt", "Pluto.txt", "Retrograde.txt", "Sagittarius.txt", "Saturn.txt", "Scorpio.txt", "Sextile.txt", "Square.txt", "Sun.txt", "Taurus.txt", "Trine.txt", "Uranus.txt", "Venus.txt", "Virgo.txt"
  ],
  server: {
    local: useLocalServer,
    baseUrl: useLocalServer ? 'http://localhost:7444' : 'https://us-central1-astrologyapi.cloudfunctions.net',
    routes: {
      event: '/event',
      events: '/events',
      natalEvents: '/natal_events',
      populateNatalEvents: '/populate_natal',
      sequentialEvents: '/sequential_events',
      zodiac: '/zodiac',
      ephemeris: '/ephemeris',
      appInfo: '/'
    }
  },
  ionic: {
    appId: '2a10d8b4',
    appVersion: "1.6.5"
  },
  google: {
    apiKey: "AIzaSyBAyZ6xYq2cR1jKqrLbakxEutzMOP0FSDk"
  }
};

export default Config;