import { PaperTemplate } from "./paper-template";
import { AstrologyEvent, Astrology } from "./astrology-event";
import { PositionsOverTime } from "./ephemeris";

import paper from 'paper';
// import moment from 'moment';

export class PaperRetrogradeGraph extends PaperTemplate {

  private signColors = [
    new paper.Color('red'),
    new paper.Color('brown'),
    new paper.Color('yellow'),
    new paper.Color('blue'),
    new paper.Color('red'),
    new paper.Color('brown'),
    new paper.Color('yellow'),
    new paper.Color('blue'),
    new paper.Color('red'),
    new paper.Color('brown'),
    new paper.Color('yellow'),
    new paper.Color('blue')
  ].map(color => { color.alpha = 0.4; return color; });

  constructor(
    scope: paper.PaperScope,
    public retrogradeEvent: AstrologyEvent,
    public directEvent: AstrologyEvent,
    public ephemeris: PositionsOverTime
  ) {
    super(scope);

    scope.project.clear();
    this.setEvent(retrogradeEvent, directEvent, ephemeris);
  }

  public redraw() {
    super.redraw();

    this.setEvent(this.retrogradeEvent, this.directEvent, this.ephemeris);
  }

  public setEvent(retrogradeEvent: AstrologyEvent, directEvent: AstrologyEvent, ephemeris: PositionsOverTime) {
    this.retrogradeEvent = retrogradeEvent;
    this.directEvent = directEvent;
    this.ephemeris = ephemeris;

    this.activate();
    this.scope.project.clear();

    this.drawGraphLine();
    this.drawAxes();
    this.drawLabels();
  }

  get planet(): string {
    return this.retrogradeEvent && this.retrogradeEvent.data.planet;
  }

  get startDate(): number { return this.ephemeris.times[0].date; }
  get endDate(): number { return this.ephemeris.times[this.ephemeris.times.length - 1].date; }

  private getRangeOfProperty(property: string, filter?: { (longitude: number, latitude: number): boolean }): { min: number, max: number } {
    const indexByField: { [field: string]: number } = {};
    this.ephemeris.fields.forEach((f, i) => indexByField[f] = i);

    const planetIndex = this.ephemeris.planets.indexOf(this.retrogradeEvent.data.planet);
    const propIndex = indexByField[property];
    
    const getField = (time: any, field: string) => time.planets[planetIndex][indexByField[field]];

    let min = null, max = null;

    this.ephemeris.times
    .filter(time => !filter || filter(getField(time, 'longitude'), getField(time, 'latitude')))
    .forEach(time => {
      const value = time.planets[planetIndex][propIndex] as number;
      if (max == null || value > max) {
        max = value;
      }
      if (min == null || value < min) {
        min = value;
      }
    })

    return { min, max };
  }

  getLatitudeRange(): { min: number, max: number } {
    return this.getRangeOfProperty('latitude');
  }

  getLongitudeRange(): { min: number, max: number } {
    return this.getRangeOfProperty('longitude');
  }

  getXValue(longitude: number): number {
    const range = this.getLongitudeRange();
    const cutoff = 180;
    const width = this.width;

    if (range.max - range.min >= cutoff) {
      
      const rangeBelowCutoff = this.getRangeOfProperty('longitude', (longitude, latitude) => longitude < cutoff);
      const rangeAboveCutoff = this.getRangeOfProperty('longitude', (longitude, latitude) => longitude >= cutoff);

      const isAfterCutoff = longitude >= cutoff;
      const longitudinalWidthBeforeCutoff = rangeBelowCutoff.max;
      const longitudinalWidthAfterCutoff = 360 - rangeAboveCutoff.min;
      const totalLongitudinalWidth = longitudinalWidthAfterCutoff + longitudinalWidthBeforeCutoff;

      const ratio = isAfterCutoff
        ? (longitude - rangeAboveCutoff.min) / totalLongitudinalWidth
        : (longitudinalWidthAfterCutoff + longitude) / totalLongitudinalWidth;
      
      const value = width * ratio;
      return value;

    } else {
      const ratio = (longitude - range.min) / (range.max - range.min);
      const value = width * ratio;
      return value;
    }
  }

  get zodiacHeight(): number { return 18; }
  get axesWidth(): number { return 38; }
  get graphYPadding(): number { return 6; }

  getYValue(latitude: number): number {
    const range = this.getLatitudeRange();
    const ratio = (latitude - range.min) / (range.max - range.min);

    const height = this.height - this.zodiacHeight - this.graphYPadding * 2;

    const value = height - (ratio * height) + this.zodiacHeight + this.graphYPadding;;

    return value;
  }

  private drawGraphLine() {
    const planetIndex = this.ephemeris.planets.indexOf(this.planet);
    const longitudeIndex = this.ephemeris.fields.indexOf('longitude');
    const latitudeIndex = this.ephemeris.fields.indexOf('latitude');

    const path = new paper.Path();
    path.strokeWidth = 1;
    path.strokeColor = new paper.Color('blue');
    this.ephemeris.times.forEach((time, index) => {
      const longitude = time.planets[planetIndex][longitudeIndex] as number;
      const latitude = time.planets[planetIndex][latitudeIndex] as number;
      
      const x = this.getXValue(longitude);
      const y = this.getYValue(latitude);

      index === 0 ? path.moveTo(new paper.Point(x, y)) : path.lineTo(new paper.Point(x, y));
      new paper.Path.Circle(new paper.Point(x, y), 1.25).fillColor = 'black'
    })

    path.smooth();
  }

  private drawAxes() {
    this.drawZodiac();
    this.drawYAxis();
  }

  private drawLabels() {
    const locs = [this.retrogradeEvent, this.directEvent]
      .map(event => ({
        event,
        retrograde: event.data.retrograde,
        position: event.getPositions().find(p => p.planet === event.data.planet)
      }))
      .map(obj => ({
        x: this.getXValue(obj.position.longitude),
        y: this.getYValue(obj.position.latitude),
        label: (obj.retrograde ? 'Rx' : 'direct') + " " + obj.position.date.format("M/D"),
        retrograde: obj.retrograde,
        event: obj.event
      }));
    
    locs
      .forEach(draw => {
        // graph dot
        const dot = new paper.Path.Circle(new paper.Point(draw.x, draw.y), 4);
        dot.fillColor = 'blue';

        // label and date
        const label = new paper.PointText(new paper.Point(0, 0));
        label.content = draw.label;
        label.fillColor = 'black';
        label.fontSize = 10;
        label.position = dot.position.add(new paper.Point((draw.retrograde ? 1 : -1) * (label.bounds.width / 2 + 6), 0))

        // vertical line with longitude
        const verticalLine = new paper.Path();
        verticalLine.moveTo(new paper.Point(draw.x, this.zodiacHeight));
        verticalLine.lineTo(new paper.Point(draw.x, this.height));
        verticalLine.strokeColor = new paper.Color('black');
        verticalLine.strokeColor.alpha = 0.5;
        verticalLine.strokeWidth = 0.5;
        verticalLine.dashArray = [1, 2];

        // label for vertical line.
        const longitudeLabel = new paper.PointText(new paper.Point(0, 0));
        const position = Astrology.getLongitudeParts(draw.event.data.longitude);
        longitudeLabel.content = `${position.degree}${Astrology.getSign(position.sign)}${position.minute}'`;
        longitudeLabel.fontFamily = 'DejaVuSans';
        longitudeLabel.fontWeight = 400;
        longitudeLabel.fontSize = 10;
        longitudeLabel.fillColor = 'black';
        longitudeLabel.position = new paper.Point(
          draw.x + longitudeLabel.bounds.width / 2 + 3,
          this.zodiacHeight + longitudeLabel.bounds.height / 2 + 3
        );
      })
  }

  private drawYAxis() {
    // draw a line at the correct location
    const line = new paper.Path();
    line.moveTo(new paper.Point(this.axesWidth, this.zodiacHeight));
    line.lineTo(new paper.Point(this.axesWidth, this.height));
    line.strokeWidth = 1;
    line.strokeColor = new paper.Color('black');

    // draw ticks at the top, bottom, and middle with the correct values
    const drawTick = (latitude: number, displayValue: number) => {
      const tick = new paper.Path();
      const yValue = this.getYValue(latitude);
      tick.moveTo(new paper.Point(this.axesWidth, yValue));
      tick.lineTo(new paper.Point(this.axesWidth - 5, yValue));
      tick.strokeWidth = 0.5;
      tick.strokeColor = 'black';
      const text = new paper.PointText(new paper.Point(0, 0));
      text.content = '' + (Math.round(Math.abs(displayValue) * 100) / 100) + (displayValue == 0 ? '' : (displayValue > 0 ? 'n' : 's'));
      text.fillColor = 'black';
      text.fontSize = 10;
      text.position = new paper.Point(this.axesWidth - 5 - text.bounds.width / 2 - 1.5, yValue);
    }

    const latitudeRange = this.getLatitudeRange();
    const diff = latitudeRange.max - latitudeRange.min;
    let step = diff / 5;
    if (diff >= 4) {
      step = 1;
    } else if (diff > 2) {
      step = 0.5;
    } else if (diff > 1) {
      step = 0.25;
    } else {
      step = 0.1;
    }

    for (let l = Math.floor(latitudeRange.min); l < latitudeRange.max; l += step) {
      drawTick(l, l);
    }
  }

  private drawZodiac() {
    // draw the zodiac.
    const range = this.getLongitudeRange();
    const cutoff = 180;
    const rangeBelowCutoff = this.getRangeOfProperty('longitude', (longitude, latitude) => longitude < cutoff);
    const rangeAboveCutoff = this.getRangeOfProperty('longitude', (longitude, latitude) => longitude >= cutoff);
    const restartsCycle = Math.abs(range.max - range.min) >= cutoff;
    const longitudinalWidthBeforeCutoff = rangeBelowCutoff.max;
    const longitudinalWidthAfterCutoff = 360 - rangeAboveCutoff.min;
    const totalLongitudinalWidth = restartsCycle ? longitudinalWidthAfterCutoff + longitudinalWidthBeforeCutoff : range.max - range.min

    const rangePx = {
      min: restartsCycle ? this.getXValue(rangeAboveCutoff.min) : this.getXValue(range.min),
      max: restartsCycle ? this.getXValue(rangeBelowCutoff.max) : this.getXValue(range.max)
    };

    const pixelsPerDegree = (this.width - this.axesWidth) / totalLongitudinalWidth;
    const pixelsPerSign = pixelsPerDegree * 30;
    const signs = Object.keys(Astrology.signCharacters);

    // how much (%) of the zodiac is before the first visible longitude value.
    const firstVisibleLongitude = restartsCycle ? rangeAboveCutoff.min : range.min;
    const zodiacProgressedRatio = firstVisibleLongitude / 360;

    // signs area
    new paper.Path.Rectangle(
      new paper.Point(rangePx.min, 0),
      new paper.Size(rangePx.max - rangePx.min, this.zodiacHeight - 4)
    );

    const topAreaRectangle = new paper.Path.Rectangle(
      new paper.Point(0, 0),
      new paper.Point(this.width, this.zodiacHeight)
    );

    for (let i = 0; i < signs.length * 2; i++) {
      const x = pixelsPerSign * i - zodiacProgressedRatio * (pixelsPerSign * 12);
      const width = pixelsPerSign;
      const height = this.zodiacHeight;
      const rect = new paper.Path.Rectangle(new paper.Rectangle(new paper.Point(x, 0), new paper.Size(width, height)));
      rect.fillColor = this.signColors[i % 12];
      const signSymbol = Astrology.signCharacters[signs[i % 12]];
      const onScreen = rect.intersect(topAreaRectangle);
      if (onScreen.bounds.intersects(this.scope.view.bounds)) {
        const text = new paper.PointText(rect.bounds.center);
        text.fontFamily = 'DejaVuSans';
        text.fontWeight = 400;
        text.fontSize = this.zodiacHeight / 2;
        text.content = signSymbol;
        text.position = onScreen.bounds.center;
        text.fillColor = 'black';
      }
    }
  }
}
