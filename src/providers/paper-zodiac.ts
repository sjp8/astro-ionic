
import * as Paper from './paper-template';
import { PaperTemplate, toCartesian } from './paper-template';
import { Astrology } from './astrology-event';
import { PlanetaryPosition } from './ephemeris';

import * as paper from 'paper';
import anime from 'animejs';
import moment from 'moment';

interface DrawPlanetComponents {
  planet: string;
  symbol: paper.PointText;
  ticks: { path: paper.Path, radius: number }[];
}

export class PaperZodiac extends PaperTemplate {

  private signSymbols: string[] = Object.keys(Astrology.signCharacters).map(k => Astrology.signCharacters[k]);

  /** Objects for each planet text. */
  private planetSets: { textRadius: number, positions: { (): PlanetaryPosition[] }, objects: DrawPlanetComponents[] }[];

  /** Objects for each aspect line. */
  private aspectObjects: paper.PathItem[];

  /** The 12 colors assigned to the zodiac signs. */
  public signColors = [
    new paper.Color('red'),
    new paper.Color('brown'),
    new paper.Color('yellow'),
    new paper.Color('blue'),
    new paper.Color('red'),
    new paper.Color('brown'),
    new paper.Color('yellow'),
    new paper.Color('blue'),
    new paper.Color('red'),
    new paper.Color('brown'),
    new paper.Color('yellow'),
    new paper.Color('blue')
  ].map(color => { color.alpha = 0.4; return color; });

  constructor(scope: paper.PaperScope, public positions: PlanetaryPosition[], public houseCusps: number[] = null, public transits: PlanetaryPosition[] = null, public emphasizedPlanets: string[] = []) {
    super(scope);
    this.setupZodiac();
  }

  public redraw() {
    super.redraw();
    this.setupZodiac();
  }

  public setPositions(positions: PlanetaryPosition[], transits: PlanetaryPosition[], getPositionForDate?: { (date: moment.Moment): PlanetaryPosition[] }) {
    // update the state of the paper canvas.
    const update = () => {
      this.updatePlanets();
      this.updateAspects();
      this.draw();
    }

    const clonePositions = positions => positions.map(p => new PlanetaryPosition(p.date, p.planet, p.longitude, p.latitude, p.distance));

    // animate separately but in parallel (transits and inner planets, whatever is present)
    [
      { name: 'positions', previousValues: this.positions, newValues: positions, setFunction: v => this.positions = v },
      { name: 'transits', previousValues: this.transits, newValues: transits, setFunction: v => this.transits = v }
    ]
      .filter(planetSet => planetSet.newValues && planetSet.previousValues).forEach(planetSet => {
        const fromDate = planetSet.previousValues[0].date.valueOf();
        const toDate = planetSet.newValues[0].date.valueOf();

        if (fromDate === toDate) {
          return;
        }

        const finalPositions = clonePositions(planetSet.newValues);

        if (!getPositionForDate) {
          planetSet.setFunction(finalPositions);
          update();
          return;
        }

        // this object holds the animated property `progress`
        const timing = { progress: fromDate }
        // perform the animation
        anime({
          targets: timing,
          progress: toDate,
          duration: 1000,
          easing: 'easeInOutQuad',
          update: () => {
            const date = moment(timing.progress);
            const positions = getPositionForDate(date);
            const positionByPlanet: { [planet: string]: PlanetaryPosition } = {};
            positions.forEach(p => positionByPlanet[p.planet] = p)
            Object.keys(positionByPlanet).forEach(planet => {
              const longitude = positionByPlanet[planet].longitude;
              const pos = positionByPlanet[planet];
              pos.longitude = longitude;
              pos.date = date;
            });
            planetSet.setFunction(positions.filter(p => positionByPlanet[p.planet]));
            update();
          },
          complete: () => {
            planetSet.setFunction(finalPositions);
            update();
          }
        });
      });
  }

  private setupZodiac() {
    console.log('setupZodiac');
    this.scope.project.clear(); // TODO avoid re-structuring the entire map.
    this.activate();

    // somehow the following prevents the chart from being displayed slightly off center.
    Paper.applyStylesToItem(new paper.Path.Rectangle(this.scope.view.bounds), {
      fillColor: 'white',
      opacity: 1
    });

    this.drawOutlines();
    this.drawHouses();
    this.setupPlanets();
    this.updateAspects();
    this.rotateChartToAC();

  }

  private drawOutlines() {
    // draw aspect circle
    
    const aspectCircle = Paper.drawCircleAtRadius(this.aspectCircleRadius, this.center);
    Paper.applyStylesToItem(aspectCircle, {
      strokeWidth: 1,
      strokeColor: 'black'
    });

    // colored background for houses
    const housesBackground = Paper.drawWedge(this.aspectCircleRadius, this.signCircleRadii[0], this.center, 360, 0);
    housesBackground.fillColor = new paper.Color('lightblue');
    housesBackground.fillColor.alpha = 0.3;
    housesBackground.sendToBack();

    // draw sign wheel
    this.signSymbols.forEach((signSymbol, index) => {
      const angle = index * 30;
      // color in the wedge for this sign.
      const wedge = Paper.drawWedge(this.signCircleRadii[0], this.signCircleRadii[1], this.center, 30, angle);
      wedge.fillColor = this.signColors[index];

      // draw the sign symbol in the wedge
      const signSymbolFontSize = (this.signCircleRadii[1] - this.signCircleRadii[0]) * 0.6;
      const text = Paper.drawTextAlongCircle((this.signCircleRadii[0] + this.signCircleRadii[1]) / 2, this.center, angle + 15, "center", "center", signSymbol, signSymbolFontSize);
      text.fillColor = 'black';
    });
    const signWheelCircles = [
      Paper.drawCircleAtRadius(this.signCircleRadii[0], this.center),
      Paper.drawCircleAtRadius(this.signCircleRadii[0] + this.tickLength, this.center),
      Paper.drawCircleAtRadius(this.signCircleRadii[1] - this.tickLength, this.center),
      Paper.drawCircleAtRadius(this.signCircleRadii[1], this.center)
    ];
    signWheelCircles.forEach(circle => circle.strokeColor = 'black');
    [0, 3].forEach(majorIndex => signWheelCircles[majorIndex].strokeWidth = 1.5);
    [1, 2].forEach(secondaryIndex => signWheelCircles[secondaryIndex].strokeWidth = 0.5);

    // draw ticks along the sign wheel.
    for (let angle = 0; angle < 360; angle++) {
      const mod10 = angle % 10 === 0;
      [0, 1].map(index => {
        const tick = Paper.drawTickAtRadius(this.signCircleRadii[index], this.center, angle, mod10 ? this.tickLength * 2 : this.tickLength, mod10 ? "center" : (index === 0 ? "inside" : "outside"));
        Paper.applyStylesToItem(tick, {
          strokeWidth: mod10 ? 1 : 0.5,
          strokeColor: 'black'
        });
      })
    }

    for (let angle = 0; angle < 360; angle += 30) {
      const line = Paper.drawLineBetweenCircles(this.signCircleRadii[0], this.signCircleRadii[1], this.center, angle);
      Paper.applyStylesToItem(line, {
        strokeWidth: 1,
        strokeColor: 'black'
      });
    }
  }

  private rotateChartToAC() {
    if (this.houseCusps) {
      return this.rotateChartToLongitude(this.houseCusps[0]);
    }
  }

  private rotateChartToLongitude(longitude: number) {
    const layer = this.scope.project.activeLayer;
    layer.applyMatrix = false;
    const rotation = Paper.normalizeAngle(longitude + 180);
    layer.rotation = rotation;

    this.planetSets.forEach(s => {
      s.objects.map(o => o.symbol).forEach(item => item.rotate(-rotation));
    });
  }

  private updateAspects() {
    // remove previous aspect lines if any
    if (this.aspectObjects) {
      this.aspectObjects.forEach(obj => obj.remove());
    }

    // draw aspects
    const aspects = Astrology.getAspects(this.positions) // retrieve aspects for given planetary positions
    const aspectLines = aspects.map(aspect => {
      const planets = aspect.indices.map(i => this.positions[i]);
      const line = Paper.drawLineBetweenAnglesAtRadius(this.aspectCircleRadius, this.center, planets[0].longitude, planets[1].longitude);
      Paper.applyStylesToItem(line, {
        strokeWidth: 1,
        strokeColor: new paper.Color(aspect.aspect.color)
      });

      return line;
    });

    this.aspectObjects = aspectLines;
  }

  private get tickLength(): number { return this.width / 120 }

  private get transitsWidth(): number { return this.width / 12 }
  private get signCircleWidth(): number { return this.width / 16 }
  private get centerCircleWidth(): number { return this.width / 16 }
  private get aspectCircleRadius(): number { return this.width / 4 }
  private get planetTextRadius(): number { return this.signCircleRadii[0] - this.planetFontSize / 1.5 - this.tickLength - 2 }
  private get outerPlanetTextRadius(): number { return this.signCircleRadii[1] + this.planetFontSize / 1.5 + this.tickLength + 2 }

  private get signCircleRadii(): number[] {
    return [
      this.width / 2 - this.transitsWidth - this.signCircleWidth, // inner circle
      this.width / 2 - this.transitsWidth // outer circle
    ]
  }

  private drawHouses() {
    this.activate();
    // draw house lines
    const centerCircle = Paper.drawCircleAtRadius(this.centerCircleWidth, this.center);
    Paper.applyStylesToItem(centerCircle, { strokeWidth: 1, strokeColor: 'black' });
    if (this.houseCusps) {
      this.houseCusps.forEach((cusp, index) => {
        const houseLine = Paper.drawLineBetweenCircles(this.centerCircleWidth, this.signCircleRadii[0], this.center, cusp);
        Paper.applyStylesToItem(houseLine, {
          strokeWidth: 1,
          strokeColor: 'black'
        });

        Paper.drawTextAlongCircle(this.centerCircleWidth, this.center, cusp + 2, "outside", "right", '' + (index + 1), this.planetFontSize * 0.5);
      });
    }
  }

  /** Update the positions of the planets within each planet set. */
  private updatePlanets() {
    this.activate();

    this.planetSets.forEach(s => {
      const { objects, positions, textRadius } = s;
      objects.forEach((obj, index) => {
        const position = positions().find(p => p.planet === obj.planet);
        // place symbol and ticks at correct longitude
        obj.symbol.position = toCartesian(textRadius, position.longitude, this.center);
        obj.ticks.forEach(tick => {
          tick.path.rotate(-tick.path.rotation, this.center); // rotate back to 0 around center.
          tick.path.rotate(-position.longitude, this.center); // rotate to new longitude.
        });
      });

      // space out planets that overlap each other.
      Paper.spaceOutItems(
        objects.map((obj, index) => ({ item: obj.symbol, angle: positions().find(p => p.planet === obj.planet).longitude })),
        textRadius,
        this.center
      );
    });
  }

  private setupPlanets() {

    this.planetSets = [];

    // draw planets and their ticks (inside, for transit charts and natal charts )
    const planetSets = [
      // natal planets
      {
        include: this.positions != null,
        textRadius: this.planetTextRadius,
        ticks: [
          { radius: this.signCircleRadii[0], position: "inside", length: this.tickLength * 2, color: 'blue' },
          { radius: this.aspectCircleRadius, position: "outside", length: this.tickLength, color: 'blue' }
        ],
        textColor: 'black',
        positions: () => this.positions
      },
      // transiting planets along outside
      { include: this.transits != null,
        textRadius: this.outerPlanetTextRadius,
        ticks: [
          { radius: this.signCircleRadii[1], position: "outside", length: this.tickLength * 2, color: 'green' }
        ],
        textColor: 'green',
        positions: () => this.transits
      }
    ];

    planetSets.filter(settings => settings.include).map(settings => {
      const objects = settings.positions().map(position => {
        const text = new paper.PointText(new paper.Point(0, 0));
        Paper.applyStylesToItem(text, {
          content: Astrology.getPlanet(position.planet),
          fontSize: this.planetFontSize,
          fillColor: settings.textColor,
          fontFamily: 'DejaVuSans'
        });
        text.applyMatrix = false;

        const ticks = settings.ticks.map(tick => {
          const path = Paper.drawTickAtRadius(tick.radius, this.center, position.longitude, tick.length, tick.position as any);
          Paper.applyStylesToItem(path, {
            strokeColor: tick.color,
            strokeWidth: 1.5
          });
          return { path, radius: tick.radius };
        });

        return { planet: position.planet, symbol: text, ticks };
      });

      this.planetSets.push({ textRadius: settings.textRadius, positions: settings.positions, objects });
    });

    this.updatePlanets();
  }


  private signWheelRadii(): { min: number, max: number } {
    // the wedges should be just about square, (so the inner radius of each sign should be the same length as
    // the width of the wedge (the difference between the inner and outer radius).
    return { min: this.radius * 0.5, max: this.radius * 0.75 };
  }

  private get planetFontSize(): number {
    const radii = this.signWheelRadii();
    return Math.max(16, 2 * Math.PI * radii.max / 12 / 6);
  }

}
