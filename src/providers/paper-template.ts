import paper from 'paper';

export class PaperTemplate {
  lastDim = { width: null, height: null };

  interval = null;
  /** Clear the resize check interval. */
  clearResizeCheckInterval() { clearInterval(this.interval); }

  constructor(public scope: paper.PaperScope) {
    this.interval = setInterval(() => {
      const view = scope.view;
      const { clientWidth, clientHeight } = view.element.parentElement;

      if ((this.lastDim.width == null || this.lastDim.height == null) || (this.lastDim.width != clientWidth || this.lastDim.height != clientHeight)) {
        this.lastDim.width = clientWidth;
        this.lastDim.height = clientHeight;
        view.draw();
        this.redraw();

      }
    }, 10);

    scope.view.onFrame = event => scope.view && scope.view.draw();
  }

  public destroy() {
    this.scope.view.onFrame = null;
    this.clearResizeCheckInterval();
    this.scope.project.clear();
    this.scope.view.remove();
    this.scope = null;
  }

  /** An abstract method to redraw the entire object graph, for example if the canvas size changes.
   * Should be overridden in any inherited implementations of PaperTemplate. */
  public redraw() {
    console.log('redraw template');
  }

  /** The center of the canvas. */
  get center(): paper.Point {
    return new paper.Point(this.scope.view.bounds.width / 2, this.scope.view.bounds.height / 2);
  }

  /** The width of the canvas. */
  get width(): number {
    return this.scope.view.size.width;
  }

  /** The height of the canvas. */
  get height(): number {
    return this.scope.view.size.height
  }

  /** The maximum radius of a circle that could fit within the canvas bounds. */
  get radius(): number {
    return Math.min(this.width, this.height) / 2;
  }

  /** Activate this PaperScope so that new paper objects will be added to its project graph. */
  public activate() {
    this.scope.activate();
    setTimeout(() => this.draw()); // after (synchronous) processing has finished, render on the next tick.
  }

  /** Shorthand for scope.view.draw() */
  public draw() {
    this.scope.view.draw();
  }

}

/** Returns a Point with the coordinates of the end of radius at the given angle. */
export const toCartesian = (radius: number, angle: number, center: paper.Point = new paper.Point(0, 0)): paper.Point =>
  new paper.Point(radius * Math.cos(angle * Math.PI / 180), -radius * Math.sin(angle * Math.PI / 180)).add(center);

/** Gets the angle in unit circle degrees for the given `point` in relation to a circle at `center`. */
export const getAngleForPoint = (center: paper.Point, point: paper.Point): number => {
  return normalizeAngle(new paper.Point(1, 0).getDirectedAngle(point));
}

/** Gets the angle for the `item`'s position in unit circle degrees for the given `point` in relation to a circle at `center`. */
export const getAngleForItem = (center: paper.Point, item: paper.Item): number => {
  return getAngleForPoint(center, item.position);
}

/** Convert angle into the equivalent angle between 0 and 359.999 degrees. */
export const normalizeAngle = (angle: number): number => {
  if (angle < 0) {
    const times = Math.floor(Math.abs(angle / 360)) + 1;
    return (angle + 360 * times) % 360;
  } else if (angle >= 360) {
    return angle % 360;
  } else {
    return angle;
  }
}

/**
 * Draws a fraction of a circle `degrees` wide, rotating it by `rotation` to place it in the unit circle.
 * It has radius `radius`, and the vertex of the arc's central angle
 * is translated to `center`. After this, it is rotated around the central vertex
 * by `rotation` degrees.
 */
export const drawArc = (radius: number, center: paper.Point, degrees: number, rotation: number): paper.Path => {
  const points = [
    new paper.Point(0, 0), // start in the middle
    new paper.Point(radius, 0), // to the right of unit circle
    toCartesian(radius, degrees / 2), // halfway through
    toCartesian(radius, degrees), // all the way through arc
    new paper.Point(0, 0) // back to origin
  ];

  const arc = new paper.Path();
  arc.moveTo(points[0]);
  arc.lineTo(points[1]);
  arc.arcTo(points[2], points[3]);
  arc.lineTo(points[4]);

  arc.closePath(true);
  arc.rotate(-rotation, new paper.Point(0, 0));
  arc.translate(center);

  return arc;
}

/** Draws a wedge (part of a donut-like wheel), with parameters similar to drawArc.  */
export const drawWedge = (innerRadius: number, outerRadius: number, center: paper.Point, angle: number, rotation: number): paper.PathItem => {
  const smallArc = drawArc(innerRadius, center, angle, rotation);
  const largeArc = drawArc(outerRadius, center, angle, rotation);
  const diffArc = largeArc.subtract(smallArc);

  return diffArc;
}

/**
 * Draws a string along the given radius and at given rotation based on center.
 * @param alignFrom The vertical line along which to orient the text (e.g. where is the path's x = 0 relative to the glyph).
*/
export const drawTextAlongCircle = (radius: number, center: paper.Point, rotation: number, position: "outside" | "inside" | "center", alignFrom: "left" | "right" | "center", str: string, fontSize: number): paper.PointText => {
  const path = new paper.PointText(new paper.Point(0, 0));
  path.applyMatrix = false;
  path.fontSize = fontSize;
  path.fontFamily = 'DejaVuSans';
  path.content = str;
  path.position = new paper.Point(0, 0);

  const { width, height } = path.bounds;
  // path is currently oriented with the left side of the letter at x = 0,
  // with the baseline of the glyph at y = 0, and the ascender at `fontSize`

  // account for position parameter.
  if (position === "inside") {
    path.translate(new paper.Point(0, height / 2));
  } else if (position === "outside") {
    path.translate(new paper.Point(0, -height / 2));
  }
  // already at the correct position if "outside".


  // account for alignFrom parameter
  if (alignFrom === "right") {
    path.translate(new paper.Point(-width / 2, 0));
  } else if (alignFrom === "left") {
    path.translate(new paper.Point(width / 2, 0));
  }

  // apply rotation around center at radius.
  path.translate(new paper.Point(0, -radius)); // radius (translate upwards)
  path.rotate(90 - rotation, new paper.Point(0, 0)); // rotation
  path.translate(center); // center

  return path;
}

/** Draws a circle with the given `radius` around point `center`. */
export const drawCircleAtRadius = (radius: number, center: paper.Point): paper.Path => {
  const path = new paper.Path.Circle(center, radius);
  return path;
}

/** Draws a tick at the given angle of a circle with `radius` and `center`, positioned inside, outside, or centered relative to the circumference line. */
export const drawTickAtRadius = (radius: number, center: paper.Point, angleInDegrees: number, length: number, position: "outside" | "inside" | "center"): paper.Path => {
  if (position === "outside") {
    return drawLineBetweenCircles(radius, radius + length, center, angleInDegrees);
  } else if (position === "center") {
    return drawLineBetweenCircles(radius - length / 2, radius + length / 2, center, angleInDegrees);
  } else if (position === "inside") {
    return drawLineBetweenCircles(radius - length, radius, center, angleInDegrees);
  }
}

/** Draws a line between two imaginary circles with radii `radius1` and `radius2` around `center`, at angle `angleInDegrees`. */
export const drawLineBetweenCircles = (radius1: number, radius2: number, center: paper.Point, angleInDegrees: number): paper.Path => {
  const line = new paper.Path();
  line.applyMatrix = false;
  line.moveTo(new paper.Point(radius1, 0));
  line.lineTo(new paper.Point(radius2, 0));
  line.translate(center);
  line.rotate(-angleInDegrees, center);

  return line;
}

/**
 * Draws a line between the given angles (standard unit circle angles), given that the center of the imaginary circle is at point `center`,
 * the endpoints of the line being `radius` away from `center`.
 */
export const drawLineBetweenAnglesAtRadius = (radius: number, center: paper.Point, angle1: number, angle2: number): paper.Path => {
  const line = new paper.Path();
  line.applyMatrix = false;
  const point1 = toCartesian(radius, angle1);
  const point2 = toCartesian(radius, angle2);
  line.moveTo(point1);
  line.lineTo(point2);
  line.translate(center);
  return line;
}

/** Applies styles given in the `styles` map to `item`. */
export const applyStylesToItem = (item: paper.Item, styles: { [style: string]: any }) => {
  Object.keys(styles).forEach(key => item[key] = styles[key]);
}

/** Given an anchored item `awayFromItem`, moves `moveItem` in `direction` away from `awayFromItem` until it is not overlapping. */
export const moveItemAwayFromItemInCircle = (moveItem: paper.Item, awayFromItem: paper.Item, center: paper.Point, direction: "clockwise" | "counterclockwise"): number => {
  let angle = getAngleForItem(center, moveItem);
  const radius = moveItem.position.getDistance(center);

  const step = direction === 'clockwise' ? -1 : 1;
  while (moveItem.bounds.intersects(awayFromItem.bounds)) {
    angle += step;
    moveItem.position = toCartesian(radius, angle, center);
  }

  return angle;
}

/**
 * Positions the `items` (corresponding to their `originalAngles`) within the wedge (portion of a circle)
 * between `angle1` and `angle2`.
 * Preserves their order while fitting them as evenly as possible, and as close to their original positions.
 */
export const positionItemsWithinWedge = (items: paper.Item[], originalAngles: number[], radius: number, center: paper.Point, angle1: number, angle2: number) => {
  const moveAway = (fromItem: paper.Item, moveThis: paper.Item): number => moveItemAwayFromItemInCircle(moveThis, fromItem, center, "counterclockwise");
  const setAngle = (item: paper.Item, angle: number) => item.position = toCartesian(radius, angle, center);

  // should be able to position every item in one iteration
  items.forEach((item, index) => {
    // move any other items that intersect away (as soon as there is no overlap, done scooting).
    items.slice(index + 1).find((otherItem, otherIndex) => {
      const intersects = item.bounds.intersects(otherItem.bounds);
      if (intersects) {
        const newAngle = moveAway(item, otherItem);
        const diff = newAngle - originalAngles[otherIndex];
        items.slice(otherIndex + 1).forEach((i, x) => setAngle(i, originalAngles[x] + diff)); // move all subsequent others the same amount
      }

      return !intersects; // stop once there is an item that does not intersect.
    });
  });
}

export const spaceOutItems = (spaceItems: { item: paper.Item, angle: number }[], radius: number, center: paper.Point): number[] => {
  const items = spaceItems.map((item, index) => ({ ...item, originalIndex: index })); // create a copy of items, keeping original index for re-sorting later.
  items.sort((a, b) => a.angle > b.angle ? 1 : -1); // sort items by angle (ascending).

  // populate a list of radial angles (how "wide" the item is in terms of degrees along the given radius).
  const getRadialAngle = (item: paper.Item): number =>(item.bounds.width + item.bounds.height) / 2 * 0.9 / (Math.PI * 2 * radius) * 360;
  const radialAngles = items.map(item => getRadialAngle(item.item));

  // populate adjusted angles with the original angles.
  const adjustedAngles = items.map(item => item.angle);

  // does items[index1] intersect with items[index2]?
  const intersects = (index1: number, index2: number): boolean => {
    const angle1 = adjustedAngles[index1];
    const angle2 = adjustedAngles[index2];
    const minDistance = radialAngles[index1] / 2 + radialAngles[index2] / 2;
    return Math.abs(angle1 - angle2) < minDistance;
  }

  // space out items starting with `startIndex`, considering that the indices contained in `groupedWidth`
  // are grouped and considered to be intersecting items.
  const space = (startIndex: number, groupedWith: number[]) => {
    const nextIndex = (startIndex === items.length - 1) ? 0 : startIndex + 1;
    const doesIntersect = intersects(startIndex, nextIndex);
    if (doesIntersect) {
      // adjust the next item to be far enough away to not intersect.
      adjustedAngles[nextIndex] = adjustedAngles[startIndex] + radialAngles[startIndex] / 2 + radialAngles[nextIndex] / 2;
      const newGroupedWith = groupedWith.concat([startIndex]);
      const groupedWithIncludingNext = newGroupedWith.concat([nextIndex]);

      // adjust to minimum distance from average angle.
      const itemCount = groupedWithIncludingNext.length;
      const averageSpaced = groupedWithIncludingNext.reduce((memo, index) => adjustedAngles[index] + memo, 0) / itemCount;
      const averageOriginal = groupedWithIncludingNext.reduce((memo, index) => items[index].angle + memo, 0) / itemCount;
      const diff = averageOriginal - averageSpaced;
      groupedWithIncludingNext.forEach(index => adjustedAngles[index] += diff);

      // continue checking for additional grouped items adjacent to nextIndex.
      space(nextIndex, newGroupedWith);
    } else if (nextIndex !== 0) {
      // no intersection, start detecting new groups
      space(nextIndex, []);
    }
  }

  // initial recursive call
  space(0, []);

  // set item locations based on results, contained in `adjustedAngles`.
  items.forEach((obj, index) => {
    obj.item.position = toCartesian(radius, adjustedAngles[index], center);
  });

  const newAngles = items.map(x => null);
  adjustedAngles.forEach((angle, index) => newAngles[items[index].originalIndex] = angle);
  return newAngles;
}
