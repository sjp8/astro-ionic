import { NgModule } from '@angular/core';
import { AstrologyEventProvider } from './astrology-event';
import { EphemerisProvider } from './ephemeris';
import { NotificationProvider } from './notification';
import { TermsProvider } from './terms';
import { SettingsProvider } from './settings';
import { GoogleProvider } from './google';
import { JournalProvider } from './journal';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LocalNotifications } from '@ionic-native/local-notifications'
import { File } from '@ionic-native/file';
import { NativeStorage } from '@ionic-native/native-storage';
import { BrowserStorage } from './browser-storage';
import { Device } from '@ionic-native/device';

@NgModule({
  providers: [
    AstrologyEventProvider,
    EphemerisProvider,
    NotificationProvider,
    TermsProvider,
    SettingsProvider,
    GoogleProvider,
    JournalProvider,
    LocalNotifications,
    SplashScreen,
    StatusBar,
    Device,
    File,
    { provide: NativeStorage, useClass: BrowserStorage }
  ]
})
export class ProvidersModule { }
