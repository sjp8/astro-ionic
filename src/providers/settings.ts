
import { Injectable } from '@angular/core';
import { NativeStorage } from '@ionic-native/native-storage';

import moment from 'moment';

@Injectable()
export class SettingsProvider {

  static SETTINGS_KEY: string = 'settings';

  static defaultSettings: Settings = {
    notifications: {
      all: true,
      planetary: true,
      moon: true,
      planetaryNatal: true,
      moonNatal: true
    },
    natal: {
      birthDate: null,
      birthLocation: null,
      correctedBirthLocation: null,
      birthLongitude: null,
      birthLatitude: null,
      birthTimezoneOffset: null
    }
  };

  constructor(private nativeStorage: NativeStorage) {}

  private async populateDefaultSettings(): Promise<Settings> {
    const defaults: Settings = { ...SettingsProvider.defaultSettings };

    await this.setSettings(defaults);
    return await this.getSettings();
  }

  async getSettings(): Promise<Settings> {
    const keys = await this.nativeStorage.keys() as string[];
    if (keys.indexOf(SettingsProvider.SETTINGS_KEY) === -1) {
      return await this.populateDefaultSettings();
    } else {
      const settings = await this.nativeStorage.getItem(SettingsProvider.SETTINGS_KEY);
      return settings || { ...SettingsProvider.defaultSettings };
    }
  }

  setSettings(settings: Settings): Promise<any> {
    return this.nativeStorage.setItem(SettingsProvider.SETTINGS_KEY, settings);
  }

  setSetting(category: string, field: string, value: string | number | boolean): Promise<any> {
    return this.getSettings()
      .then(settings => {
        settings[category][field] = value;
        return this.setSettings(settings);
      });
  }

  async getSetting(category: string, field: string): Promise<string | number | boolean> {
    const settings = await this.getSettings();
    return settings[category][field];
  }

  public async getNatalBirthDate(natalSettings?: NatalSettings): Promise<moment.Moment> {
    const settings = natalSettings || (await this.getSettings()).natal;
    if (settings.birthDate) {
      const dateStr = settings.birthDate + (settings.birthTimezoneOffset ? getUTCOffsetString(settings.birthTimezoneOffset) : '');
      return moment(dateStr);
    } else {
      return null;
    }
  }
}

/** Settings related to any natal features and data. */
export interface NatalSettings {
  birthDate: string
  birthLocation: string
  correctedBirthLocation: string
  birthLongitude: number
  birthLatitude: number
  birthTimezoneOffset: number
}

/** All settings related to receiving notifications. */
export interface NotificationSettings {
  all: boolean
  planetary: boolean
  moon: boolean
  planetaryNatal: boolean
  moonNatal: boolean
}

export interface Settings {
  notifications: NotificationSettings
  natal: NatalSettings
}

export const getUTCOffsetString = (offsetInSeconds: number): string => {
  const pad = (num: number, sign: boolean = false) => (sign ? (num < 0 ? '-' : '+') : '') + (Math.abs(num) < 10 ? `0${Math.abs(num)}` : `${Math.abs(num)}`);
  const hours = (offsetInSeconds < 0 ? Math.ceil : Math.floor)(offsetInSeconds / 60 / 60);
  const minutes = Math.round(Math.abs(offsetInSeconds % 3600) / 60);
  const utcOffsetString = `${pad(hours, true)}:${pad(minutes, false)}`;
  return utcOffsetString;
}
