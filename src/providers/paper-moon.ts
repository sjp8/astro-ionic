
import paper from 'paper';
import moment from 'moment';
import { AstrologyEvent } from './astrology-event';
import { PaperTemplate, toCartesian } from './paper-template';

export class PaperMoon extends PaperTemplate {
  static alternatePhases = {
    'Crescent': 'Waxing Crescent',
    'Gibbous': 'Waxing Gibbous',
    'Disseminating': 'Waning Gibbous',
    'Balsamic': 'Waning Crescent'
  };

  static phases = {
    'New': { text: 'New', angle: 0, illuminatedSide: "right", illuminatedFraction: 0 },
    'Waxing Crescent': { text: 'Waxing Crescent', alternateText: 'Crescent', angle: 45, illuminatedSide: "right", illuminatedFraction: 0.25 },
    'First Quarter': { text: 'First Quarter', angle: 90, illuminatedSide: "right", illuminatedFraction: 0.5 },
    'Waxing Gibbous': { text: 'Waxing Gibbous', alternateText: 'Gibbous', angle: 135, illuminatedSide: "right", illuminatedFraction: 0.75 },
    'Full': { text: 'Full', angle: 180, illuminatedSide: "left", illuminatedFraction: 1 },
    'Waning Gibbous': { text: 'Waning Gibbous', alternateText: 'Disseminating', angle: 225, illuminatedSide: "left", illuminatedFraction: 0.75 },
    'Third Quarter': { text: 'Third Quarter', angle: 270, illuminatedSide: "left", illuminatedFraction: 0.5 },
    'Waning Crescent': { text: 'Waning Crescent', alternateText: 'Balsamic', angle: 315, illuminatedSide: "left", illuminatedFraction: 0.25 }
  };

  getPhaseData(phaseName: string) {
    return PaperMoon.phases[phaseName] || PaperMoon.phases[PaperMoon.alternatePhases[phaseName]];
  }

  discsByPhase: { [phase: string]: paper.Group } = {}
  descriptionsByPhase: { [phase: string]: paper.Item } = {};

  constructor(scope: paper.PaperScope, public phases: AstrologyEvent[], public moonEvents: AstrologyEvent[]) {
    super(scope);
    this.setPhases(phases, moonEvents);
  }

  get phaseCycleRadius(): number {
    return this.radius * 0.5;
  }

  get phaseCircleRadius(): number {
    return this.phaseCycleRadius / 8;
  }

  get fontSize(): number {
    return this.radius / 22;
  }

  public redraw() {
    super.redraw();
    this.setPhases(this.phases, this.moonEvents);
  }

  public setPhases(phases: AstrologyEvent[], moonEvents: AstrologyEvent[]) {
    this.phases = phases || this.phases;
    this.moonEvents = moonEvents || this.moonEvents;

    this.scope.project.clear(); // todo avoid re-structuring the entire map.
    this.activate();

    this.drawSun();
    this.drawCycleCircle();
    this.setupPhaseCircles();
    this.setupPhaseDescriptions();
    this.setupMoonEvents();
    this.drawEarth();
  }

  private drawSun() {
    const path = new paper.Path();
    path.moveTo(this.scope.view.bounds.topRight);
    path.arcTo(this.scope.view.bounds.rightCenter.add(new paper.Point(-this.width / 8, 0)), this.scope.view.bounds.bottomRight);
    path.closePath(false);
    path.fillColor = new paper.Color('#FFED00');
    path.fillColor.alpha = 0.8;
  }

  private drawEarth(): paper.Path {
    const earth = new paper.Path.Circle(this.calculateCenter(), this.phaseCircleRadius * 1.4);
    earth.fillColor = new paper.Color('blue');
    earth.strokeColor = new paper.Color('green');
    earth.strokeWidth = this.phaseCircleRadius / 3;

    return earth;
  }

  private drawCycleCircle(): paper.Path[] {
    // setup overall cycle line (big circle) with varying opacity based on time from now
    // (now is solid black, later is increasingly transparent).
    const phaseCycleCircle = new paper.Path.Circle(this.center, this.phaseCycleRadius);
    phaseCycleCircle.flatten(0.25);
    phaseCycleCircle.smooth();
    phaseCycleCircle.reverse();
    phaseCycleCircle.rotate(-180 - this.getAngleForDate(this.phases[0].date));
    const numSegments = phaseCycleCircle.segments.length;
    const circleSegments = phaseCycleCircle.segments.map((segment, i) => {
      const nextSegmentIndex = i === numSegments - 1 ? 0 : i + 1;
      const nextSegment = phaseCycleCircle.segments[nextSegmentIndex];
      const path = new paper.Path([segment, nextSegment]);
      path.strokeColor = new paper.Color('black');
      path.strokeWidth = 1;
      path.opacity = 1 - Math.min(1, (i / numSegments * 1.1)); // should go from 1 to ~0.1 opacity over the course of circle.
      return path;
    });

    return circleSegments;
  }

  private getAngleForDate(date: moment.Moment): number {
    const angle = this.phases.map((event, index) => {
      const nextEvent = this.phases[index + 1];
      if (nextEvent && date.isSameOrAfter(event.date) && date.isBefore(nextEvent.date)) {
        const ea = this.getPhaseData(event.data.phase).angle;
        const nea = this.getPhaseData(nextEvent.data.phase).angle;
        const ratio = (date.valueOf() - event.date.valueOf()) / (nextEvent.date.valueOf() - event.date.valueOf());
        const angle = ratio * ((nea < ea ? nea + 360 : nea) - ea) + ea;
        return angle;
      }
    }).find(angle => angle != null); // return first angle that worked.

    return angle;
  }

  /** Draw lines for apogee, perigee, AN, and DN. */
  private setupMoonEvents() {
    const types = {
      an: '☊ AN',
      dn: '☋ DN',
      perigee: 'perigee',
      apogee: 'apogee'
    }
    const events = this.moonEvents.filter(e => Object.keys(types).indexOf(e.data.type) !== -1);


    const lines = events.map((event, i) => {
      const nextEvent = events[i + 1];
      const angle = this.getAngleForDate(event.date);
      const nextEventAngle = nextEvent ? this.getAngleForDate(nextEvent.date) : 1000;
      const line = new paper.Path();
      line.strokeColor = ['perigee', 'apogee'].indexOf(event.data.type) !== -1 ? 'blue' : 'green';
      line.strokeWidth = 1;
      line.dashArray = [1, 2];
      const center = this.calculateCenter();
      const edgePoint = toCartesian(this.phaseCycleRadius, angle).add(this.center);
      const distance = center.getDistance(edgePoint);
      line.moveTo(center);
      line.lineTo(edgePoint);

      const textRotation = edgePoint.subtract(center).angle;

      const text = new paper.PointText(new paper.Point(0, 0));
      text.fontSize = 10;
      text.applyMatrix = false;
      text.content = types[event.data.type] + ' ' + event.date.format('M/D');
      text.position = center;
      text.translate(new paper.Point(
        text.bounds.width / 2 + distance - text.bounds.width - this.phaseCircleRadius,
        (nextEventAngle - angle < 20 ? 1 : -1) * text.bounds.height / 2 + 1
      ));
      text.rotation = 0;
      text.rotate(textRotation, center);
      if (angle > 90 && angle < 270) {
        text.rotation = text.rotation + 180;
      }
      return line;
    })

    return lines;
  }

  /** Calculate the center given moon events (position earth closer to perigee or farther from apogee, whichever is first). */
  private calculateCenter(): paper.Point {
    const first = this.moonEvents.find(moon => ['perigee', 'apogee'].indexOf(moon.data.type) !== -1 && moon.date.isSameOrAfter(this.phases[0].date));
    if (!first) {
      return this.center;
    }
    const angle = this.getAngleForDate(first.date);
    const translation = toCartesian(this.phaseCycleRadius / 8, angle + (first.data.type === 'perigee' ? 0 : 180));
    return this.center.add(translation);
  }

  /** Setup illuminated discs on the outside of the circle (moon phases). */
  private setupPhaseCircles() {
    this.activate();

    const phaseCircles = Object.keys(PaperMoon.phases).map(p => {
      const data = this.getPhaseData(p);
      const phase = data.text;
      const circle = this.drawPhaseCircle(data.illuminatedSide, data.illuminatedFraction, this.phaseCycleRadius, data.angle);
      this.discsByPhase[phase] = circle;
      return circle;
    })

    return phaseCircles;
  }

  private getEventByPhase(phaseName: string): AstrologyEvent {
    const data = this.getPhaseData(phaseName);
    const event = this.phases.find(p => data.alternateText ? p.data.phase === data.alternateText : p.data.phase === data.text);
    return event;
  }

  private getPhaseOpacity(phaseEvent: AstrologyEvent): number {
    const index = this.phases.findIndex(p => p.databaseId === phaseEvent.databaseId);
    const opacity = 1 - (index * 1/16);
    return isNaN(opacity) ? 1 : opacity;
  }

  private setupPhaseDescriptions() {
    this.activate();
    const texts = Object.keys(PaperMoon.phases).map(p => {
      const data = this.getPhaseData(p);
      const circle = this.discsByPhase[data.text];
      const event = this.getEventByPhase(data.text);
      const opacity = this.getPhaseOpacity(event);
      // const content = data.text.replace(/[ ]/g, "\n") + "\n" + event.date.format('MMM D') + "\n" + event.date.format("h:mm a");
      const content = data.text + "\n" + event.date.format('MMM D h:mm a');
      const text = this.drawTextOutsidePhaseCircle(data.angle, circle, content);
      text.opacity = opacity;
      circle.opacity = opacity;

      this.descriptionsByPhase[data.text] = text;
      return text;
    })
    
    return texts;
  }

  drawTextOutsidePhaseCircle(angle: number, phaseCircle: paper.Item, content: string): paper.PointText {
    const text = new paper.PointText(new paper.Point(0, 0));
    text.content = content;
    text.fontSize = this.fontSize;
    text.fillColor = new paper.Color('black');

    const positioning: { position: string, textAlign: string } = ({
      0: { position: "right", textAlign: "left" },
      45: { position: "right", textAlign: "left" },
      90: { position: "top", textAlign: "center" },
      135: { position: "left", textAlign: "right" },
      180: { position: "left", textAlign: "right" },
      225: { position: "left", textAlign: "right" },
      270: { position: "bottom", textAlign: "center" },
      315: { position: "right", textAlign: "left" }
    })[angle];

    text.justification = positioning.textAlign;

    const offset = this.phaseCircleRadius * 1.3;
    const { width, height } = text.bounds.scale(0.5);

    switch (positioning.position) {
      case "right":
        text.position = phaseCircle.position.add(new paper.Point(offset + width, 0));
        break;
      case "left":
        text.position = phaseCircle.position.add(new paper.Point(-offset - width, 0));
        break;
      case "bottom":
        text.position = phaseCircle.position.add(new paper.Point(0, offset + height));
        break;
      case "top":
        text.position = phaseCircle.position.add(new paper.Point(0, -offset - height));
        break;
    }

    return text;
  }

  drawPhaseCircle(lightSide: "right" | "left", lightFraction: number, radius: number, angle: number): paper.Group {
    const position = this.center.add(toCartesian(radius, angle));

    const circleRadius = this.phaseCircleRadius;
    const circle = new paper.Path.Circle(new paper.Point(0, 0), circleRadius);
    circle.fillColor = new paper.Color('black');
    circle.strokeColor = new paper.Color('black');
    circle.strokeWidth = 0.5;
    circle.translate(position);

    const lightCircle = new paper.Path();
    lightCircle.fillColor = new paper.Color('white');
    lightCircle.moveTo(new paper.Point(0, -circleRadius)); // start at top
    const outerMidpoint = lightSide === "right" ? new paper.Point(circleRadius, 0) : new paper.Point(-circleRadius, 0);
    const innerMidpointX = (circleRadius * 2 * lightFraction - circleRadius) * (lightSide === "left" ? 1 : -1);
    const innerMidpoint = new paper.Point(innerMidpointX, 0);
    lightCircle.arcTo(outerMidpoint, new paper.Point(0, circleRadius)); // arc around outside of moon
    lightCircle.arcTo(innerMidpoint, new paper.Point(0, -circleRadius)); // arc around boundary between illuminated and dark.
    lightCircle.translate(position);

    return new paper.Group([circle, lightCircle]);
  }
}