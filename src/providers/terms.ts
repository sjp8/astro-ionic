import { Injectable } from '@angular/core';
import { Observable } from 'rxjs'
import { Http } from '@angular/http';

import Config from '../providers/config';

@Injectable()
export class TermsProvider {

  constructor(private http: Http) { }

  static dictionary: TermDictionary;

  getDictionary(): Observable<TermDictionary> {
    if (TermsProvider.dictionary) {
      return Observable.of(TermsProvider.dictionary);
    } else {
      return Observable.forkJoin(Config.termFiles.map(file => this.http.get("../assets/data/definitions/" + file)))
        .map(responses => {
          const contents = responses.map(res => res.text());
          const terms = Config.termFiles.map(file => file.replace(".txt", ""));
          const dictionary = new TermDictionary();
          contents.forEach((content, index) => {
            dictionary.addFile(terms[index], content);
          })
          TermsProvider.dictionary = dictionary;
          return dictionary;
        })
    }
  }
}

export class TermDictionary {
  terms: { [term: string]: Term } = {};
  
  addFile(name: string, content: string): Term {
    const term = Term.fromFile(name, content);
    term.names.forEach(name => {
      this.terms[name] = term;
    });

    return term;
  }

  getTermObject(name: string): Term {
    return this.terms[name];
  }

}

export class Term {

  constructor(
    /** Possible strings by which this term can be looked up. */
    public names: string[],

    /** Ways of describing this term (not limited to adjectives). */
    public adjectives: string[],

    /** Facts associated with this term. */
    public facts: string[],

    /** Adjectives for this term which show up only when connected with the given connected term. */
    public adjectivesByTerm: { [connectedTerm: string]: string[] },

    /** Articles related to the term. */
    public articles: string[]
  ) { }

  hasName(name: string): boolean {
    return this.names && this.names.indexOf(name) !== -1;
  }

  getAdjectives(connectedTerms: string[]): string[] {
    const connectedAdjectives = [];
    connectedTerms.forEach(connected => {
      if (this.adjectivesByTerm[connected]) {
        connectedAdjectives.push(...this.adjectivesByTerm[connected]);
      }
    });
    return this.adjectives.concat(connectedAdjectives);
  }

  static fromFile(name: string, content: string): Term {
    const lines = content.split("\n").map(line => line.trim()).filter(line => line);

    // aliases start with @.
    const names = [name].concat(lines.filter(line => line.startsWith('@')).map(line => line.substring(1)));
    // adjectives are everything else.
    const adjectives = lines.filter(line => line.search(/^[A-Za-z0-9]/) !== -1);
    // facts (astronomical for instance) start with *.
    const facts = lines.filter(line => line.startsWith('*')).map(line => line.substring(1));
    // articles start with $.
    const articles = lines.filter(line => line.startsWith('$')).map(line => line.substring(1));
    // adjectives by term start with &, and if multiple terms are specified, they are separated with ,
    // the terms and the adjectives are separated by =
    const adjectivesByTerm = {}; lines
      .filter(line => line.startsWith('&')).map(line => line.substring(1))
      .map(line => line.split('=').map(s => s.trim()))
      .forEach(parts => {
        const terms = parts[0].split(',').map(p => p.trim());
        terms.sort((a, b) => a > b ? 1 : -1);
        const adjective = parts[1];
        const k = terms.join(",");
        adjectivesByTerm[k] = adjectivesByTerm[k] || [];
        adjectivesByTerm[k].push(adjective);
      });
    
    const term = new Term(names, adjectives, facts, adjectivesByTerm, articles);

    return term;
  }


}
