
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { SettingsProvider } from './settings';

import * as moment from 'moment';
import { AstrologyEvent } from './astrology-event';

const testEvent = new AstrologyEvent('aspect', {"_id":"5aeab9850f561e4ffacfc357","planets":[{"_id":"5aeab9850f561e4ffacfc359","name":"Moon","longitude":277.6947652059133,"retrograde":false},{"_id":"5aeab9850f561e4ffacfc358","name":"Saturn","longitude":277.6948110973773,"retrograde":true}],"type":"aspect","date":"2018-06-01T00:52:43.000Z","aspect":"Conjunction","major":true,"applying":null,"planetsSeparating":false,"exact":true,"__v":0});
const testEvent2 = new AstrologyEvent('sign', {"_id":"5aeabac00135f3504a056a9b","type":"sign","date":"2018-06-02T22:06:16.000Z","planet":"Moon","sign":"Aquarius","longitude":300.00006043885776,"retrograde":false,"__v":0,"ephemeris":{"planets":["Sun","Moon","Mercury","Mars","Venus","Jupiter","Saturn","Uranus","Neptune","Pluto","Chiron"],"longitudes":[72.31176637023013,300.00006043885776,68.40238823848227,305.8834755630328,107.08993686818194,225.39627334567137,277.5784992708079,30.914618402961185,346.4218465558323,290.8975078389882,1.9747043790775056],"latitudes":[-0.000025753359506716242,0.638994732331089,0.2039649105555205,-2.998858606038383,1.9279062769406239,1.2111538251083873,0.8775893044864318,-0.5270095202347467,-0.9487665513502173,0.2301733008861567,3.4721132492768376],"longitudeSpeeds":[0.9578077365457554,11.785433624419845,2.1768313044063725,0.26399867127842847,1.1811383340183912,-0.10199677603760708,-0.06256669504092413,0.04620639027648785,0.008772613184632034,-0.01768727030218572,0.02719493266859171],"distances":[1.0142242657480394,0.0027090013457668903,1.3154638961845002,0.5996347883791474,1.2586835660466784,4.480471299352317,9.138989779644115,20.635454355693124,29.997625215730917,32.77533256596131,18.996293743801658]}})
const testData: JournalEntry[] = [
  { eventId: testEvent.databaseId, event: testEvent, items: [
      { type: 'text', content: 'Journal Entry Text Example...' },
      { type: 'image', content: 'https://upload.wikimedia.org/wikipedia/commons/d/de/C._c._bromia%2C_in_New_York%2C_NY.jpg' },
      { type: 'video', content: 'https://upload.wikimedia.org/wikipedia/commons/e/ec/Blue_jay_-_nut_cracking.ogv' },
      { type: 'audio', content: 'https://upload.wikimedia.org/wikipedia/commons/1/15/Cyanocitta_cristata_-_Blue_Jay_-_XC86756.ogg' }
    ], date: moment().toDate() },
  { eventId: testEvent2.databaseId, event: testEvent2, items: [
      { type: 'text', content: 'Another journal entry\n\nSpanning Several Lines.' },
      { type: 'text', content: 'Second Item' }
    ], date: moment().subtract(5, 'days').toDate() },
];

export interface JournalEntry {
  items: { type: 'text' | 'video' | 'audio' | 'image', content: string }[];
  date: Date
  eventId: string
  event: any
}

@Injectable()
export class JournalProvider {
  constructor(private http: Http, private settings: SettingsProvider) { }

  async  getJournalEntries(): Promise<JournalEntry[]> {
    return testData;
  }

  async getJournalEntry(eventId: string): Promise<JournalEntry> {
    const entries = await this.getJournalEntries();
    const entry = entries.find(entry => entry.eventId === eventId);
    return entry;
  }

  async createJournalEntry(event: AstrologyEvent): Promise<JournalEntry> {
    const entry: JournalEntry = {
      items: [],
      date: new Date(),
      eventId: event.databaseId,
      event: event.data
    };

    return entry;
  }

  async addItem(type: 'text' | 'video' | 'audio' | 'image', content: string, entry: JournalEntry): Promise<JournalEntry> {
    const clone = { ...entry, items: entry.items.map(i => ({ ...i })) };
    clone.items.push({ type, content });
    return clone;
  }
}
