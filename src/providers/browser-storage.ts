
import { NativeStorage } from '@ionic-native/native-storage';
import { Injectable } from '@angular/core';

@Injectable()
export class BrowserStorage extends NativeStorage {
  setItem(reference: string, value: any): Promise<any> {
    window.localStorage.setItem(reference, JSON.stringify(value));
    return Promise.resolve();
  }
  getItem(reference: string): Promise<any> {
    return Promise.resolve(JSON.parse(window.localStorage.getItem(reference)));
  }
  keys(): Promise<any> {
    const result = [];
    for (let i = 0; i < window.localStorage.length; i++) {
      result.push(window.localStorage.key(i));
    }
    return Promise.resolve(result);
  }
  remove(reference: string): Promise<any> {
    window.localStorage.removeItem(reference);
    return Promise.resolve();
  }
  clear(): Promise<any> {
    window.localStorage.clear();
    return Promise.resolve();
  }
}