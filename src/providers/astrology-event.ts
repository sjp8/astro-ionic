
import { Injectable } from '@angular/core'
import { Http, Response } from '@angular/http'

import { PlanetaryPosition } from './ephemeris'
import Config from './config';

import * as moment from 'moment'
import { Observable } from 'rxjs'

@Injectable()
export class AstrologyEventProvider {

  constructor(public http: Http) {
  }

  eventCache: { [eventId: string]: AstrologyEvent } = {};
  getEvent(eventId: string, type: string): Observable<AstrologyEvent> {
    if (this.eventCache[eventId]) {
      return Observable.of(this.eventCache[eventId]);
    }

    const params = {
      id: eventId
    };

    const url = Config.server.baseUrl + Config.server.routes.event

    return this.http.get(url, { params })
      .map(res => res.json())
      .map(json => this.parseEventJSON(json.event, type))
      .do(event => this.eventCache[eventId] = event)
  }

  /** Get natal events for the given birth configuration, and period of time. */
  public getNatalEvents(birthDate: moment.Moment, latitude: number, longitude: number, fromDate: moment.Moment, toDate: moment.Moment): Observable<AstrologyEvent[]> {
    if (!birthDate || !fromDate || !toDate) { debugger }
    const params = {
      from: fromDate.toISOString(),
      to: toDate.toISOString(),
      latitude, longitude,
      birthDate: birthDate.toISOString()
    };

    const url = Config.server.baseUrl + Config.server.routes.natalEvents;

    return this.http.get(url, { params })
      .map(res => res.json())
      .map(json => this.parseEventsJSON(json))
  }

  /**
   * Get events from the data source in the given time range. If toDate is not specified, it defaults to 1 month after fromDate.
   * If fromDate is not specified, defaults to now.
   * 
   * @param{moment.Moment} fromDate From this date.
   * @param{moment.Moment} toDate To this date.
   */
  getEvents(fromDate?: moment.Moment, toDate?: moment.Moment, type?: string): Observable<AstrologyEvent[]> {
    fromDate = fromDate || moment.utc()
    toDate = toDate ? toDate : moment.utc(fromDate).add(1, 'month')

    const params = {
      from: fromDate.toISOString(),
      to: toDate.toISOString(),
      type
    };

    const url = Config.server.baseUrl + Config.server.routes.events
    return this.http.get(url, { params })
      .map(res => res.json())
      .map(json => this.parseEventsJSON(json, type));
  }

  getExampleEvents(fromDate?: moment.Moment, toDate?: moment.Moment): Observable<AstrologyEvent[]> {
    return this.http.get('assets/data/example_notifications.json')
      .map(res => res.json())
      .map(json => this.parseEventsJSON(json));
  }

  getSequentialEvents(aroundEventId: string, type: string, countBefore: number = 4, countAfter: number = 4): Observable<AstrologyEvent[]> {
    const params = {
      eventId: aroundEventId,
      type: type,
      countBefore: '' + countBefore,
      countAfter: '' + countAfter
    };

    const url = Config.server.baseUrl + Config.server.routes.sequentialEvents
    return this.http.get(url, { params })
      .map(res => res.json())
      .map(json => json.events.map(e => this.parseEventJSON(e, type)))
  }
  
    
  private natalEventLog: { birthDate: number, latitude: number, longitude: number }[] = [];

  public populateNatalEvents(birthDate: moment.Moment, latitude: number, longitude: number): Observable<{ longitude: number, latitude: number, birthDate: number }> {
    const log = this.natalEventLog.find(l => l.latitude === latitude && l.longitude === longitude && l.birthDate === birthDate.unix());
    if (log) {
      return Observable.of(null);
    }

    this.natalEventLog.push({ birthDate: birthDate.unix(), latitude, longitude });
    const url = Config.server.baseUrl + Config.server.routes.populateNatalEvents;
    const params = {
      longitude,
      latitude,
      birthDate: birthDate.toISOString()
    }

    return this.http.get(url, { params })
      .map(res => res.json())
      .map(json => ({
        longitude: json.longitude as number,
        latitude: json.latitude as number,
        birthDate: json.birthDate as number
      }))
  }

  parseEventsJSON(json: any, type?: string): AstrologyEvent[] {
    return json
      .map(event => this.parseEventJSON(event, type || event.type))
      .filter(event => event && !event.invalid)
  }

  parseEventJSON(eventJSON: any, type: string): AstrologyEvent {
    const event = new AstrologyEvent(type, eventJSON)
    return event
  }

}

export class AstrologyEvent {
  static types: string[] = ['aspect', 'phase', 'sign', 'retrograde', 'moon', 'natal']
  static typeIcons: string[] = ['medical', 'contrast', 'planet', 'git-compare', 'moon', 'planet']

  invalid: boolean = false
  type: string
  date: moment.Moment
  icon: string
  databaseId: string

  data: any

  constructor(type: string, data: any) {
    this.type = type
    this.icon = (data.planet == 'Moon' && type != 'phase') ? 'moon' : AstrologyEvent.typeIcons[AstrologyEvent.types.indexOf(this.type)]
    this.data = data
    this.date = moment(data.date)
    this.databaseId = data._id

    if (AstrologyEvent.types.indexOf(type) === -1) {
      this.invalid = true
    }
  }

  get id(): number {
    return AstrologyEvent.types.indexOf(this.type) + 10 * this.date.valueOf()
  }

  getPositions(): PlanetaryPosition[] {
    const ephemeris = this.data.ephemeris
    if (!ephemeris) {
      return null;
    }

    const { planets, longitudes, longitudeSpeeds, latitudes, distances } = ephemeris;
    const date = this.date;
    return planets.map((p, i) => new PlanetaryPosition(date, p, longitudes[i], latitudes[i], distances[i], longitudeSpeeds[i]));
  }

  getDescription(utf8: boolean, stripHtml: boolean = false): string {
    let getItemWithFunction = (func: { (arg: any): any }) => {
      return (str: string) => {
        return utf8 ? func(str) : str
      }
    }
    const intermediate = () => {
      let phase = getItemWithFunction(Astrology.getPhase)
      let sign = getItemWithFunction(Astrology.getSign)
      let planet = getItemWithFunction(Astrology.getPlanet)
      let aspect = getItemWithFunction(Astrology.getAspect)
      let moonEvent = (type) => {
        const types = {
          an: utf8 ? '☊' : 'Ascending Node',
          dn: utf8 ? '☋' : 'Descending Node',
          perigee: 'Perigee',
          apogee: 'Apogee',
          voc: utf8 ? 'VOC' : 'Void of Course'
        }
        return types[type] || ('unknown' && console.error('Unknown moon phase type: ' + type))
      }

      const e = this.data
      if (this.type == 'aspect') {
        //const direction = e.exact ? 'exact' : (e.entered ? 'entering' : 'leaving')
        const retrogradeStr = utf8 ? '<sub>r</sub>' : ' Rx'
        let retrogrades = [0, 1].map(index => e.planets[index].retrograde === true ? retrogradeStr : '')
        return `${planet(e.planets[0].name)}${retrogrades[0]} ${aspect(e.aspect)} ${planet(e.planets[1].name)}${retrogrades[1]}`
      } else if (this.type == 'sign') {
        let transition = utf8 ? '→' : ' enters '
        let retrograde = e.retrograde ? '<sub>r</sub>' : ''
        return `${planet(e.planet)}${retrograde}${transition}${sign(e.sign)}`
      } else if (this.type == 'phase') {
        let space = utf8 ? '' : ' '
        let phasePlanet = utf8 ? '' : planet(e.planet)
        return `${phase(e.phase)}${space}${phasePlanet}`
      } else if (this.type == 'retrograde') {
        const retrograde = e.retrograde ? (utf8 ? '<sub>r</sub>' : ' retrograde') : (utf8 ? '<sub>d</sub>' : ' direct')
        return `${planet(e.planet)}${retrograde}`
      } else if (this.type == 'moon') {
        let transition = utf8 ? '→' : ' reaches '
        if (e.type == 'apogee' || e.type == 'perigee') {
          return `${planet('Moon')} ${transition} ${e.type}`
        } else if (e.type == 'an' || e.type == 'dn') {
          return `${planet('Moon')} ${transition} ${moonEvent(e.type)}`
        }
      } else if (this.type === 'natal') {
        if (e.event === 'aspect') {
          const aspectType = Astrology.getAspectForAngle(Astrology.getAngle(e.transitingPlanet.longitude, e.natalPlanet.longitude))
          const retrogradeStr = utf8 ? '<sub>r</sub>' : ' Rx'
          const natalStr = utf8 ? ' Nat. ' : ' Natal '
          const fields = ['transitingPlanet', 'natalPlanet']
          const planets = fields.map(f => e[f])
          const retrogrades = fields.map(t => e[t].retrograde ? retrogradeStr : '')
          return `${planet(planets[0].name)}${retrogrades[0]} ${aspect(aspectType)}${natalStr}${planet(planets[1].name)}${retrogrades[1]}`
        } else if (e.event === 'house') {
          const descStr = utf8 ? '' : ' enters house '
          return `${planet(e.transitingPlanet.name)}${descStr}${e.natalHouse}`
        }
      } else {
        return null
      }
    }

    const result = intermediate();
    return result && (stripHtml ? result.replace(/<[^>]*>/g, '') : result);
  }

  get textDescription(): string { return this.getDescription(false) }
  get description(): string { return this.getDescription(true) }
  get planets(): string[] {
    if (this.data.type === 'natal') {
      return [this.data.transitingPlanet.name].concat(this.data.natalPlanet ? [this.data.natalPlanet.name] : []);
    } else if (this.data.type === 'aspect') {
      return this.data.planets.map(x => x.name);
    } else if (this.data.type === 'moon') {
      return ['Moon'];
    } else {
      return [this.data.planet] || [];
    }
  }

  getTerms(): string[] {
    const capitalize = (str: string): string => str[0].toUpperCase() + str.slice(1);

    const data = this.data;
    const terms = [];
    terms.push(...this.planets);
    data.aspect && terms.push(data.aspect);
    data.phase && terms.push(data.phase);
    data.type === 'moon' && terms.push(capitalize(data.event as string));
    data.type === 'moon' && terms.indexOf('Moon') === -1 && terms.push('Moon');
    data.sign && terms.push(data.sign);
    data.retrograde != null && terms.push(data.retrograde ? 'Retrograde': 'Direct');

    return terms;
  }
}

export interface AspectDefinition {
  name: string
  angle: number
  color: string
  orb: number
  major: boolean
}

export interface AspectInfo {
  indices: number[]
  aspect: AspectDefinition
  angle: number
  separation: number
  applying: boolean
}

export class Astrology {

  static getPlanet(name: string): string { return Astrology.planetCharacters[name] || name }
  static getSign(name: string): string { return Astrology.signCharacters[name] || name }
  static getAspect(name: string): string { return Astrology.aspectCharacters[name] || name }
  static getPhase(name: string): string { return Astrology.phaseCharacters[name] || name }
  static getAlternatePhase(name: string): string { return Astrology.alternatePhaseNames[name] || name }
  static isMajorAspect(aspect: string): boolean { return Astrology.majorAspects.indexOf(aspect) !== -1 }
  static getAspectDefinition(name: string): AspectDefinition {
    return Astrology.aspects.find(a => a.name === name);
  }

  static getAngle(longitude1: number, longitude2: number): number {
    return Math.min(360 - Math.abs(longitude1 - longitude2), Math.abs(longitude1 - longitude2));
  }

  static getAspects(planets: PlanetaryPosition[], previousPlanets?: PlanetaryPosition[]): AspectInfo[] {
    const longitudes = planets.map(p => p.longitude)
    const previousLongitudes = previousPlanets && previousPlanets.map(p => p.longitude)

    const result = [];
    for (var l = 0; l < longitudes.length; l++) {
      for (var n = l + 1; n < longitudes.length; n++) {
        Astrology.aspects.forEach(aspect => {
          const planetaryDiff = [longitudes, previousLongitudes].filter(x => x).map(
            values => Astrology.getAngle(values[n], values[l])
          )

          const aspectDiff = planetaryDiff.map(diff => Math.abs(aspect.angle - diff))

          if (aspectDiff[0] < aspect.orb) {
            result.push({
              indices: [l, n],
              aspect: aspect,
              angle: planetaryDiff[0],
              separation: aspectDiff[0],
              applying: aspectDiff[1] ? aspectDiff[0] < aspectDiff[1] : null
            });
          }
        });
      }
    }

    return result;
  }

  static getAspectForAngle(angle: number): string {
    const aspect = Astrology.aspects.find(a => {
      const diff = Math.abs(a.angle - angle)
      return diff <= a.orb;
    });

    return aspect && aspect.name
  }

  static getElementForSign(sign: string): 'Fire' | 'Earth' | 'Air' | 'Water' {
    const elements = ['Fire', 'Earth', 'Air', 'Water']
    const signIndex = Object.keys(Astrology.signCharacters).indexOf(sign)
    return elements[signIndex % 4] as any;
  }

  static getColorForSign(sign: string): string {
    const colorByElement = {
      earth: "#d2691e",
      fire: "#ff0000",
      water: "#8fb3be",
      air: "#b8b800"
    };

    const element = Astrology.getElementForSign(sign).toLocaleLowerCase();
    return colorByElement[element];
  }

  static getLongitudeParts(longitude: number): LongitudeParts {
    // how many times does 30 go into the planet longitude?
    const signIndex = Math.floor(longitude / 30)
    const signs = Object.keys(Astrology.signCharacters)
    const sign = signs[signIndex]
    const degreeDec = longitude - signIndex * 30
    const degree = Math.floor(degreeDec)
    const hour = Math.floor(degreeDec) // how far are we through this 30 degree arc
    const minutesWithDecimal = (degreeDec - hour) * 60
    const minute = Math.floor(minutesWithDecimal) // how far through this degree (as a portion of 60)
    const second = Math.floor((minutesWithDecimal - minute) * 60) // how far through the portion of 60 (as a portion of 60)

    return { sign, degree, hour, minute, second } // "hour" and "degree" are equivalent
  }

  /** Get the next phase after the given `phase`. */
  static getNextPhase(phase: string): string {
    const names = [], alternates = []
    for (var p in Astrology.alternatePhaseNames) {
      names.push(p)
      alternates.push(Astrology.alternatePhaseNames[p])
    }

    const pIndex = names.indexOf(phase)
    const aIndex = names.indexOf(phase);

    const results = [pIndex, aIndex]
      .filter(index => index !== -1)
      .map(index => (index + 1) % names.length)

    return results.length ? names[results[0]] : null
  }

  static moonNodeCharacters: { [key: string]: string } = {
    an: '☊',
    dn: '☋',
    apogee: '⚸',
    perigee: 'P'
  }

  static phaseCharacters: { [key: string]: string } = {
    "New": "🌑", "Crescent": "🌒", "First Quarter": "🌓", "Gibbous": "🌔", "Full": "🌕", "Disseminating": "🌖", "Third Quarter": "🌗", "Balsamic": "🌘"
  }

  static alternatePhaseNames: { [key: string]: string } = {
    "New": "New",
    "Crescent": "Waxing Crescent",
    "First Quarter": "First Quarter",
    "Gibbous": "Waxing Gibbous",
    "Full": "Full",
    "Disseminating": "Waning Gibbous",
    "Third Quarter": "Third Quarter",
    "Balsamic": "Waning Crescent"
  }

  static signCharacters: { [key: string]: string } = {
    "Aries": "♈",
    "Taurus": "♉",
    "Gemini": "♊",
    "Cancer": "♋",
    "Leo": "♌",
    "Virgo": "♍",
    "Libra": "♎",
    "Scorpio": "♏",
    "Sagittarius": "♐",
    "Capricorn": "♑",
    "Aquarius": "♒",
    "Pisces": "♓"
  }

  static planetCharacters: { [key: string]: string } = {
    "Sun": "☉",
    "Moon": "☽",
    "Mercury": "☿",
    "Mars": "♂",
    "Venus": "♀",
    "Jupiter": "♃",
    "Saturn": "♄",
    "Uranus": "♅",
    "Neptune": "♆",
    "Pluto": "♇",
    "Chiron": "⚷",
    "Ceres": "⚳",
    "Pallas": "⚴",
    "Juno": "⚵",
    "Vesta": "⚶"
  }

  static aspectCharacters: { [key: string]: string } = {
    "Square": "□",
    "Sextile": "✱",
    "Trine": "△",
    "Opposition": "☍",
    "Conjunction": "☌",
    "Semisquare": "Semisquare",
    "Sesquisquare": "⚼",
    "Semisextile": "⚺",
    "Quincunx": "⚻",
    "Quintile": "Q",
    "Biquintile": "bQ"
  }

  static aspects: AspectDefinition[] = [
    { name: 'Conjunction', angle: 0, color: 'blue', orb: 10, major: true },
    { name: 'Sextile', angle: 60, color: 'blue', orb: 6, major: true },
    { name: 'Square', angle: 90, color: '#b8b800', orb: 10, major: true },
    { name: 'Trine', angle: 120, color: 'blue', orb: 10, major: true },
    { name: 'Opposition', angle: 180, color: '#b8b800', orb: 10, major: true }
  ]

  static majorAspects: string[] = ["Square", "Sextile", "Trine", "Opposition", "Conjunction"]

}

export interface LongitudeParts {
  sign: string
  degree: number
  hour: number
  minute: number
  second: number
}
