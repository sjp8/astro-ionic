
import { PaperTemplate } from "./paper-template";
import { AstrologyEvent, Astrology } from "./astrology-event";
import { PositionsOverTime } from "./ephemeris";

import paper from 'paper';
import moment from 'moment';

export class PaperAspectGraph extends PaperTemplate {

  constructor(
    scope: paper.PaperScope,
    public aspectEvent: AstrologyEvent,
    public sequentialEvents: AstrologyEvent[],
    public retrogrades: AstrologyEvent[],
    public ephemeris: PositionsOverTime
  ) {
    super(scope);

    this.setEvent(aspectEvent, sequentialEvents, retrogrades, ephemeris);
  }

  public redraw() {
    super.redraw();

    this.setEvent(this.aspectEvent, this.sequentialEvents, this.retrogrades, this.ephemeris);
  }

  public setEvent(aspectEvent: AstrologyEvent, sequentialEvents: AstrologyEvent[], retrogrades: AstrologyEvent[], ephemeris: PositionsOverTime) {
    this.aspectEvent = aspectEvent;
    this.ephemeris = ephemeris;

    this.scope.project.clear();
    this.activate();

    this.drawAxes();
    this.drawGraphLine();
    this.drawAspectAreas();
    this.drawGraphLineLabels();

    this.draw();
  }

  get planets(): { name: string, longitude: number, retrograde: boolean }[] {
    return this.aspectEvent && this.aspectEvent.data.planets;
  }

  get startDate(): number { return this.ephemeris.times[0].date; }
  get endDate(): number { return this.ephemeris.times[this.ephemeris.times.length - 1].date; }

  private getAngleRange(): { min: number, max: number } {
    const indexByField: { [field: string]: number } = {};
    this.ephemeris.fields.forEach((f, i) => indexByField[f] = i);

    const planetIndices = this.planets.map(p => this.ephemeris.planets.indexOf(p.name));
    const propIndex = indexByField['longitude'];

    const getAngle = (time: any) => this.getAngle(time.planets[planetIndices[0]][propIndex], time.planets[planetIndices[1]][propIndex]);

    let min = null, max = null;

    this.ephemeris.times
      .forEach(time => {
        const angle = getAngle(time);
        if (max == null || angle > max) {
          max = angle;
        }
        if (min == null || angle < min) {
          min = angle;
        }
      })

    const result = { min, max };
    return result;
  }

  get axesWidth(): number { return 38; }
  get outerMargin(): number { return 24; }

  getYValue(angle: number): number {
    const range = this.getAngleRange();

    const ratio = 1 - (angle - range.min) / (range.max - range.min);
    const availableHeight = this.height - 2 * this.outerMargin;
    const offset = this.outerMargin;
    return ratio * availableHeight + offset;
  }

  getXValue(date: moment.Moment) {
    const width = this.width - this.axesWidth;
    const ratio = (date.valueOf() - this.startDate.valueOf()) / (this.endDate.valueOf() - this.startDate.valueOf());
    return ratio * width + this.axesWidth;
  }

  private getAngle(longitude1: number, longitude2: number): number {
    const diff = Math.abs(360 + longitude1 - longitude2) % 360;
    return diff > 180 ? 360 - diff : diff;
  }

  private drawGraphLineLabels() {
    this.sequentialEvents && this.sequentialEvents.map(event => {
      const angle = this.getAngle(event.data.planets[0].longitude, event.data.planets[1].longitude);
      const y = this.getYValue(angle);
      const x = this.getXValue(event.date);
      if (x < this.axesWidth || x > this.width) {
        return;
      }

      // draw point on graph line
      const point = new paper.Path.Circle(new paper.Point(x, y), 2);
      if (event.databaseId === this.aspectEvent.databaseId) {
        point.fillColor = 'black';
      } else {
        point.strokeColor = 'black';
        point.strokeWidth = 0.5;
      }
      
      // draw date label next to graph line
      const label = new paper.PointText(new paper.Point(0, 0));
      label.fontSize = 12;
      label.content = event.date.format('M/D');
      label.position = new paper.Point(x, y + label.bounds.height / 2 + 2);
      (label as any).shadowBlur = 4;
      (label as any).shadowColor = new paper.Color('white');

      // draw vertical line through point
      const line = new paper.Path();
      line.moveTo(new paper.Point(x, 0));
      line.lineTo(new paper.Point(x, this.height));
      line.strokeColor = 'black';
      line.dashArray = [1, 3];
      line.strokeWidth = 0.5;

      // draw longitude labels for each planet
      event.data.planets.forEach((planet,  index) => {
        const location = Astrology.getLongitudeParts(planet.longitude);
        const color = new paper.Color(Astrology.getColorForSign(location.sign));
        const texts = [
          { content: location.degree },
          { content: Astrology.getSign(location.sign), fontFamily: 'DejaVuSans', fontWeight: 300 }
        ].map(attrs => {
          const text = new paper.PointText(new paper.Point(0, 0));
          Object.keys(attrs).forEach(a => text[a] = attrs[a]);
          text.fontSize = 12;
          text.fillColor = color;
          const labelY = index === 0 ? this.outerMargin / 2 : this.height - this.outerMargin / 2
          // const labelY = index === 0 ? this.outerMargin - text.bounds.height / 2 - 2 : this.height - this.outerMargin + text.bounds.height / 2 + 2;
          text.position = new paper.Point(x + text.bounds.width / 2 + 2, labelY);
          return text;
        });

        texts[1].position = texts[0].position.add(new paper.Point(texts[0].bounds.width / 2 + texts[1].bounds.width / 2, 0));
      })
    });
  }

  private drawGraphLine() {
    const planetIndices = this.planets.map(p => this.ephemeris.planets.indexOf(p.name));
    const longitudeIndex = this.ephemeris.fields.indexOf('longitude');

    const getAngle = (time: any) => this.getAngle(time.planets[planetIndices[0]][longitudeIndex], time.planets[planetIndices[1]][longitudeIndex]);

    const path = new paper.Path();
    path.strokeWidth = 1;
    path.strokeColor = new paper.Color('blue');
    this.ephemeris.times.forEach((time, index) => {
      const angle = getAngle(time);

      const x = this.getXValue(moment(time.date));
      const y = this.getYValue(angle);

      index === 0 ? path.moveTo(new paper.Point(x, y)) : path.lineTo(new paper.Point(x, y));
    })

    path.smooth();
  }
 
  private drawAspectAreas() {
    const { min: minAngle, max: maxAngle } = this.getAngleRange();

    const drawLine = (y: number, color: string) => {
      const line = new paper.Path()
      line.moveTo(new paper.Point(this.axesWidth, y));
      line.lineTo(new paper.Point(this.width, y));
      line.strokeColor = new paper.Color(color);
      line.strokeColor.lightness = 0.2;
      line.strokeWidth = 0.5;
    }

    const drawAspect = (angle: number, orb: number, color: string, symbol: string) => {
      console.debug('drawing aspect', symbol);
      const y = this.getYValue(angle);
      drawLine(y, color);

      // draw transparent rectangle signifying orb
      const rect = new paper.Path.Rectangle(
        new paper.Point(this.axesWidth, this.getYValue(Math.max(minAngle, angle - orb))),
        new paper.Point(this.width, this.getYValue(Math.min(maxAngle, angle + orb)))
      );
      rect.fillColor = new paper.Color(color);
      rect.fillColor.alpha = 0.3;
    }

    Astrology.aspects
      .filter(aspect => aspect.angle + aspect.orb >= minAngle && aspect.angle - aspect.orb <= maxAngle)
      .forEach(aspect =>
        drawAspect(aspect.angle, aspect.orb, aspect.color, Astrology.getAspect(aspect.name))
      );
  }

  private drawAxes() {
    // draw planet lines, and y-axis.
    const above = new paper.Path();
    above.moveTo(new paper.Point(0, this.outerMargin));
    above.lineTo(new paper.Point(this.width, this.outerMargin));

    const below = new paper.Path();
    below.moveTo(new paper.Point(0, this.height - this.outerMargin));
    below.lineTo(new paper.Point(this.width, this.height - this.outerMargin));

    const left = new paper.Path();
    left.moveTo(new paper.Point(this.axesWidth, this.outerMargin));
    left.lineTo(new paper.Point(this.axesWidth, this.height - this.outerMargin));

    [above, below, left].forEach(path => {
      path.strokeColor = 'black';
      path.strokeWidth = 1;
    });

    left.strokeColor = 'red';

    // draw planet symbols next to y-axis
    const planets = this.aspectEvent.data.planets.map(p => p.name);
    planets.forEach((planet, index) => {
      const symbol = Astrology.getPlanet(planet);
      const text = new paper.PointText(new paper.Point(0, 0));
      text.content = symbol;
      text.fontSize = 16;
      text.fontFamily = 'DejaVuSans';
      text.fontWeight = 300;
      text.position = new paper.Point(
        this.axesWidth / 2,
        index === 0 ? this.outerMargin / 2 : this.height - this.outerMargin / 2
      );
    });

    // draw ticks on y-axis
    const range = this.getAngleRange();
    const minAngle = Math.floor((range.min + 10) / 10) * 10;
    const step = 10;
    const symbolByAngle = {};
    Astrology.aspects.forEach(a => symbolByAngle[a.angle] = Astrology.getAspect(a.name));

    for (let a = minAngle; a < range.max; a += step) {
      const tick = new paper.Path();
      const y = this.getYValue(a);
      tick.moveTo(new paper.Point(this.axesWidth, y));
      tick.lineTo(new paper.Point(this.axesWidth - 4, y));
      tick.strokeColor = 'black';
      tick.strokeWidth = 0.5;

      const symbol = symbolByAngle['' + a] || null;
      const labels = [
        { content: '' + a },
        { content: symbol || '', fontFamily: 'DejaVuSans', fontWeight: 300 }
      ];
      const texts = labels.map(attrs => {
        const text = new paper.PointText(new paper.Point(0, 0));
        Object.keys(attrs).filter(attr => attrs[attr]).forEach(attr => text[attr] = attrs[attr]);
        text.fontSize = 10;
        text.fillColor = 'black';
        text.position = new paper.Point(this.axesWidth - 6 - text.bounds.width / 2, y);
        return text;
      });

      texts[1].position = texts[0].position.subtract(new paper.Point(texts[0].bounds.width / 2 + texts[1].bounds.width / 2 + 2, 0));
    }
  }
}
