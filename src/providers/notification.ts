
import { Injectable } from '@angular/core'
import { AstrologyEvent, AstrologyEventProvider } from './astrology-event'
import { EventDetailPage } from '../pages/event-detail/event-detail'
import { Platform, NavController } from 'ionic-angular'
import { LocalNotifications, ILocalNotification } from '@ionic-native/local-notifications'

import * as moment from 'moment/moment'

import { Settings, getUTCOffsetString } from './settings';

@Injectable()
export class NotificationProvider {

  constructor(public platform: Platform, private localNotifications: LocalNotifications, private eventProvider: AstrologyEventProvider) {
  }

  async getAllNotifications(): Promise<ILocalNotification[]> {
    return await this.localNotifications.getAllScheduled();
  }

  async scheduleNotifications(navCtrl: NavController, settings: Settings) {
    await this.platform.ready();
    if (!this.platform.is('cordova')) {
      return false;
    }

    await this.localNotifications.cancelAll();

    if (!settings.notifications.all) {
      return;
    }

    const natalDataAvailable = settings.natal.birthLatitude && settings.natal.birthLongitude && settings.natal.birthDate && settings.natal.birthTimezoneOffset;
    const includeNatalEvents = (settings.notifications.moonNatal || settings.notifications.planetaryNatal) && natalDataAvailable;
    const includeCelestialEvents = settings.notifications.moon || settings.notifications.planetary;

    const fromDate = moment.utc().startOf('day');
    const toDate = fromDate.clone().add(2, 'month');

    const natalEvents = includeNatalEvents ? (
      await this.eventProvider.getNatalEvents(
        moment(settings.natal.birthDate + getUTCOffsetString(settings.natal.birthTimezoneOffset)),
        settings.natal.birthLatitude, settings.natal.birthLongitude, fromDate, toDate).toPromise()
      ) : [];
    
    const transitEvents = includeCelestialEvents ? (
      await this.eventProvider.getEvents(fromDate, toDate).toPromise()
    ) : [];

    const events = natalEvents.concat(transitEvents)
      .filter(event => event.date.isAfter(moment())) // only events after now.
      .filter(event => { // only events matching the given Notification Settings
          const conditions: boolean[] = [];
          if (settings.notifications.moon) {
            conditions.push(event.type != 'natal' && event.planets.indexOf('Moon') !== -1);
          }
          if (settings.notifications.planetary) {
            conditions.push(event.type != 'natal' && event.planets.indexOf('Moon') === -1);
          }
          if (settings.notifications.moonNatal) {
            conditions.push(event.type === 'natal' && event.planets.indexOf('Moon') !== -1);
          }
          if (settings.notifications.planetaryNatal) {
            conditions.push(event.type === 'natal' && event.planets.indexOf('Moon') === -1);
          }

          return conditions.some(condition => condition);
      })
    
    console.log('settings', JSON.stringify(settings.notifications, null, "\t"));
    console.log('events after filtering', events.map(e => e.getDescription(true, true)));
    
    const scheduled = await this.localNotifications.getAllScheduled();
    const scheduledIds = scheduled.map(n => n.id);
    const scheduledMessages = scheduled.map(n => n.text);
    const eventsToSchedule = [];
    const eventsToCancel = [];
    events.forEach((event, i) => {
      // message for the notification
      const message = event.getDescription(false, true);

      // determine whether to skip, replace a notification, or create a notification.
      const scheduledIndex = scheduledIds.indexOf(event.date.valueOf());
      const scheduledMessage = scheduledMessages[scheduledIndex];
      const alreadyScheduled = scheduledIndex !== -1;
      const messageChanged = alreadyScheduled && message !== scheduledMessage;

      // cancel the existing message if the message changed.
      if (messageChanged) {
        eventsToCancel.push(scheduledIds[scheduledIndex]);
      }
      if (!alreadyScheduled || messageChanged) {
        console.log('scheduling ' + message + ' at ' + event.date.format())
        eventsToSchedule.push({
          text: message,
          at: moment.utc().add(eventsToSchedule.length * 3, 'seconds').toDate(),
          id: event.date.valueOf(),
          data: {
            type: 'event',
            data: event.data
          }
        });
      }
    });

    await this.localNotifications.schedule(eventsToSchedule);
    await Promise.all(eventsToCancel.map(notificationId => this.localNotifications.cancel(notificationId)));
    await this.setClickHandler(navCtrl);

    return true;
  }

  /**
   * Ensures that permission for notifications has been granted, returning the result
   * as a boolean promise.
   */
  public async ensureHasPermission(): Promise<boolean> {
    await this.platform.ready();
    if (this.platform.is('cordova')) {
      const hasPermission = await this.localNotifications.registerPermission();
      return hasPermission;
    } else {
      return false;
    }
  }

  private async setClickHandler(navCtrl: NavController) {
    await this.platform.ready();

    await this.localNotifications.on('click', arg => {
      const notificationData = JSON.parse(arg.data);
      const eventData = notificationData.data;
      if (notificationData.type === 'event' && eventData) {
        const event = new AstrologyEvent(eventData.type, eventData);
        navCtrl.push(EventDetailPage, { event });
      } else {
        console.error('no eventdata or the click was not a result of an event-type notification.');
      }
    })
  }
}
