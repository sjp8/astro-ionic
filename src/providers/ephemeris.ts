import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs'

import { Astrology, LongitudeParts } from './astrology-event'
import Config from './config'

import * as ephemeris from 'ephemeris-moshier'

import * as moment from 'moment';
import memoize from 'memoizerific';

@Injectable()
export class EphemerisProvider {

  constructor(public http: Http) { }

  getMoshierPositionsBefore(date: moment.Moment): PlanetaryPosition[] {
    const before = moment(date.valueOf()).add(-1, 'minute')
    return this.getMoshierPositions(before)
  }

  private positions = memoize(100)((date: number): PlanetaryPosition[] => {
    const dateMoment = moment.utc(date);
    const ephemerisDate = dateMoment.format('DD[.]MM[.]YYYY HH:mm:ss')
    const planets = ephemeris.getAllPlanets(ephemerisDate, 10.0014, 53.5653, 0)
    const result = Object.keys(planets.observed)
      .map(name => name[0].toUpperCase() + name.substr(1))
      .filter(name => Object.keys(Astrology.planetCharacters).indexOf(name) !== -1)
      .map(name => {
        const data = planets.observed[name.toLowerCase()]
        data.planet = name
        return data
      })
      .map(data => new PlanetaryPosition(dateMoment, data.planet, data.apparentLongitudeDd, data.geocentricDistanceKm === -1 ? null : data.geocentricDistanceKm))

    return result
  });

  getMoshierPositions(date: moment.Moment): PlanetaryPosition[] {
    return this.positions(date.valueOf());
  }

  getMoshierPositionForPlanet(planet: string, date: moment.Moment): PlanetaryPosition {
    return this.getMoshierPositions(date).find(p => p.planet == planet);
  }

  getPositions(date: moment.Moment, longitude: number, latitude: number): Observable<PlanetaryPosition[]> {

    const params = {
      date: date.toISOString(),
      latitude, longitude
    };

    const url = Config.server.baseUrl + Config.server.routes.zodiac
    return this.http.get(url, { params })
      .map((response: Response) => this.parsePositions(response.json(), date))
  }

  getHouses(date: moment.Moment, longitude: number, latitude: number): Observable<number[]> {
    const params = {
      date: date.toISOString(),
      longitude, latitude
    };

    const url = Config.server.baseUrl + Config.server.routes.zodiac
    return this.http.get(url, { params })
      .map(res => res.json())
      .map(json => json.houses.map(h => h.cusp as number));
  }

  parsePositions(json: any, date: moment.Moment): PlanetaryPosition[] {
    return json.display.map(pos => new PlanetaryPosition(date, pos.planet, pos.longitude))
  }

  getEphemeris(from: moment.Moment, to: moment.Moment, stepUnit: string, planets: string[] = null): Observable<PositionsOverTime> {
    const params: any = {
      from: from.toISOString(),
      to: to.toISOString(),
      step: stepUnit
    };

    if (planets) {
      params.planets = planets.join(",");
    }

    const url = Config.server.baseUrl + Config.server.routes.ephemeris
    
    return this.http.get(url, { params })
      .map(res => res.json())
  }

  getMoshierPhase(date: moment.Moment): MoonPhase {
    const positions = this.getMoshierPositions(date);
    const sun = positions.find(p => p.planet === 'Sun');
    const moon = positions.find(p => p.planet === 'Moon');

    return MoonPhase.fromLongitudes(date, sun.longitude, moon.longitude);
  }
}

/** Contains an ephemeris from the server for a given date range */
export class PositionsOverTime {
  fields: string[]
  planets: string[]
  times: {
    date: number,
    planets: (number | boolean)[][]
  }[]
}

export class PlanetaryPosition {
  constructor(
    public date: moment.Moment,
    public planet: string,
    public longitude: number,
    public latitude: number = null,
    public distance: number = null,
    public longitudeSpeed: number = null
  ) { }

  hasLatitude(): boolean { return this.latitude != null }
  hasDistance(): boolean { return this.distance != null }

  get position(): LongitudeParts {
    return Astrology.getLongitudeParts(this.longitude)
  }

  get retrograde(): boolean { return this.longitudeSpeed == null ? null : this.longitudeSpeed < 0; }
}

export class MoonPhase {
  constructor(
    public date: moment.Moment,
    public fullAngle: number
  ) { }

  get name(): string {
    const index = Math.floor(this.fullAngle / 45);
    const names = Object.keys(Astrology.alternatePhaseNames);
    // console.log('index ' + index + ', name: ' + names[index]);
    return names[index];
  }

  isWaxing(): boolean { return this.fullAngle < 180; }
  isWaning(): boolean { return !this.isWaxing(); }
  partialAngle(): number { return this.isWaning() ? 360 - this.fullAngle : this.fullAngle; }

  /** The ratio of illuminated area over total area. (Does not take into account latitude.) */
  illuminatedPortion(): number {
    return this.partialAngle() / 180;
  }

  darkPortion(): number { return 1 - this.illuminatedPortion(); }

  lightIsOnLeft(): boolean { return this.isWaning(); }
  lightIsOnRight(): boolean { return this.isWaxing(); }

  static fromLongitudes(date: moment.Moment, sunLongitude: number, moonLongitude: number): MoonPhase {
    // the sun's longitude is always increasing (except when jumping from 359.99 to 0)
    // the moon's longitude is also always increasing. However the moon moves faster than the sun.
    // therefore, the moon is waxing (moving toward an opposition) if the moon is ahead of the sun
    //    ahead of means "the angle of the moon minus the angle of the sun is less than 180 degrees"
    //    otherwise, the moon is behind the sun and moving toward a new moon (waning).
    const fullAngle = ((moonLongitude + 360) - sunLongitude) % 360;
    return new MoonPhase(date, fullAngle);
  }
}